﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Indexing.v3;
using Google.Apis.Indexing.v3.Data;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Muranosoft.Indexing
{
    public static class IndexingApiHelper
    {
        public enum PublishType { Update = 0, Delete = 1 }
        private static readonly string[] PublishTypeWords = { "URL_UPDATED", "URL_DELETED" };

        private static string b64IndexingCreds = "ewogICJ0eXBlIjogInNlcnZpY2VfYWNjb3VudCIsCiAgInByb2plY3RfaWQiOiAibXVyYW5vc29mdHJ1LTE0OTQ5Mjg5Njk2MjQiLAogICJwcml2YXRlX2tleV9pZCI6ICJhZTNhOTE0ZGI4ZGE3ZDExNmY2YmYzNWQ0ZDBlZTFhYjU4MTNkYTU2IiwKICAicHJpdmF0ZV9rZXkiOiAiLS0tLS1CRUdJTiBQUklWQVRFIEtFWS0tLS0tXG5NSUlFdlFJQkFEQU5CZ2txaGtpRzl3MEJBUUVGQUFTQ0JLY3dnZ1NqQWdFQUFvSUJBUUNQMVdyaXRmRk9iS2krXG5OTVI2TUkzZnQ1RXFOb3kzZXB4d1BVSk4zMC9INTFTeUlCbTUyTkdUM210SXhuNWhGS3RFaDh0LzR3elY0SUpmXG4yWDNuWkpvRUVWc3hldVZRTU1NVHhYUWpFZFp0TWIwaUNrcy93ak1ZWlpVdXZDOXNoQkRnWitZYXlwS1RlUW9BXG55M1hWdzNUQ2NrTlFuR05RZ201QzVTUmVVT1VpUnZ5K21RR0dLSmIwZ2pxRUhrdlc1WE9BWWV5N3VLczB4Q2J3XG5QMmI3aWV5R2RpbVhoVkFJOEIxZ0FYTUF2SENXaEcxQzVVU01GS2RqdzhJVGJLV0lVakZsamZ4OW14RDZ0eDUzXG5RMW1hNHZrK0hLemdMZ0hkTUJmVWE4Y0xCRy8vNGNab3JpbFh2aXUzNDM3Y3BqYzhOZDBWT096djB0aXByR0RvXG5qZUNMSzF1SkFnTUJBQUVDZ2dFQU9WOWlUc05QV1Q1N2dyOXVuZVRQT3EwdzBOdElmd2wvN0Q5ck9yKzRSWjl2XG5SOHdKSnZvbkF1b2w5allEQkR0bjQ2S2RHRXFiZkphY3VwbjVJRHNmT2o4b0ZwRVVka3UxREtabTRIUkZpWEhkXG5KUDVoTExJTWlvOCtsaEZIZzJ3Y2hUNjhhUFJqeWtKdUxJbjcyMFFuUjBhbnRqaGpYeUpmMC93dDZSY3dodDZiXG42bzVxc3BCMzR4dUlqeEVtaTdtNzJ5djNlWXZXWjVlNUJaNHlSbThmUUkyVHE5UWRNWnE3bDh3OVdmSW1zc0JVXG5PQUVFQ3VDR0U0eWVDRlA0bFVNSW1BU0svWnBvRU5zaEF5M0Q4djArVitIcmRLT1RJVWZNTDdZNXJDOWhHYXdWXG5OVGNUVkZBREZCNSswcm1vb0JRQm95eDNLZFBwYkNBSzZKcDlVcFV4andLQmdRRENaYmZFeEhvZk44NUV4d2lBXG5qczV5WW54dkZFbEE2YTJhZGFwSjh3c2t0MkcrdUkzR1dZakNVZndaakpqNU9HUVhMcEk0dFRiMVFzelc1aGdaXG5pS1dUWExaaFcwbjVFaDZCVmk4WHAzQTluTHVoN3BxWUlTNEsva3pHUldEbFk1b3NhY1JLMXFYRDJLa0JJZG9JXG5YdE00VUJjTlJJY0xsdkdYN0NCREVtZjdid0tCZ1FDOWFjWXVuR2lFajJSbXJ2MjhuM2JDRk1HdFNwRUZGdmJmXG5aRUhzazZ3ZW52SFRyMUpuMGI5OVRoUGU2ekMyOEdDaTdqUVNLK1JQdEhad3dkQis5aytvOTc5WUZ6anp6THZLXG5nRnhsUEs2ak1pczJoclRCVEhWUk9zbWp5aXJpd0NZdUFJbFE1OUd1eTZSY3ZzT05XQTNrNVpXNFRJam1zaURaXG4wRXJ5K2pSOGh3S0JnUUNYRVJiVEdXQlJ2RHRXSlZuTkZhMEhKTlBRT25LR3VGR0prK0Rtb0xITE1ac0s3RDU0XG5vb0Z0aU9xaExDcUFtYnJtYlBHQ2tnaUlaZ1hyQmxvNkZYK2VXZEQ5TFVUNkFvT2tQTVpSMm5PVjNhUE9QaTd6XG5BKzNVNnpQY2JIUFZMeWJUVnNvTTRtdGxHNHlTYmEwVERpV1VRR0Q1bFlSZ2xFZGdxVVdVK2N2R2p3S0JnRkR5XG5xMmV3SUZyR2pUbjAveXRsV3ZPR3UxNjd4MDVOaWlQZVRPNDJEaERvREtGVEEveXFmeE9tRW9mcUsxK0RTMGxXXG5VNXFRZDNvNHZZVml6eC96VkFmNkdTUWtpSXBvckxqV01xWnhVSkVlRjZtdDJBSGtFeDhyNkpDL0RhVHFTV2JQXG5nOSszc3J6bytzMUlHWHFHYnltcmxGaTNIS3E2NVo4d2pWZFo1cVJ4QW9HQVN4YnBQT2gvSTFwMU1haWhwMmlZXG42V09EcW40aDZvYS9IS1pDT0drb2tvMkdCbWFVUVFzRDl1ekhmVkRCOUhQeGxGWlg2YzdHOEdQQ1FKKzQrRnprXG5kdW1yQ3pOZzJrNjBER2xadmtNd0crUWt4UTNVSVhta3BvYjJ3VGd1UGFsUlg3V3ExMHZCR0hFdkJVbXVwYmVnXG5najRGVk5IcHVxRWtHZTdYY2JlK1o4UT1cbi0tLS0tRU5EIFBSSVZBVEUgS0VZLS0tLS1cbiIsCiAgImNsaWVudF9lbWFpbCI6ICJtdXJhbm9zb2Z0cnUtaW5kZXhpbmdAbXVyYW5vc29mdHJ1LTE0OTQ5Mjg5Njk2MjQuaWFtLmdzZXJ2aWNlYWNjb3VudC5jb20iLAogICJjbGllbnRfaWQiOiAiMTA0MTYzNjUxMTA4NDk4MDY0OTgzIiwKICAiYXV0aF91cmkiOiAiaHR0cHM6Ly9hY2NvdW50cy5nb29nbGUuY29tL28vb2F1dGgyL2F1dGgiLAogICJ0b2tlbl91cmkiOiAiaHR0cHM6Ly9vYXV0aDIuZ29vZ2xlYXBpcy5jb20vdG9rZW4iLAogICJhdXRoX3Byb3ZpZGVyX3g1MDlfY2VydF91cmwiOiAiaHR0cHM6Ly93d3cuZ29vZ2xlYXBpcy5jb20vb2F1dGgyL3YxL2NlcnRzIiwKICAiY2xpZW50X3g1MDlfY2VydF91cmwiOiAiaHR0cHM6Ly93d3cuZ29vZ2xlYXBpcy5jb20vcm9ib3QvdjEvbWV0YWRhdGEveDUwOS9tdXJhbm9zb2Z0cnUtaW5kZXhpbmclNDBtdXJhbm9zb2Z0cnUtMTQ5NDkyODk2OTYyNC5pYW0uZ3NlcnZpY2VhY2NvdW50LmNvbSIKfQo=";
        private static GoogleCredential GetGoogleCredential()
        {
            using var stream = new MemoryStream(System.Convert.FromBase64String(b64IndexingCreds));
            return GoogleCredential.FromStream(stream).CreateScoped(new[] { "https://www.googleapis.com/auth/indexing" });
        }

        public static string UrlNotificationToString(UrlNotification notification)
        {
            return (notification == null)
                ? "null"
                : $"[{notification.NotifyTime}] Type: {notification.Type}, Url: {notification.Url}";

        }
        public static string PublishUrlNotificationResponseToString(PublishUrlNotificationResponse response)
        {
            var sb = new StringBuilder();
            if (response == null)
            {
                sb.Append("Response is empty");
            }
            else
            {
                sb.Append("Last update: ");
                sb.AppendLine(UrlNotificationToString(response.UrlNotificationMetadata.LatestUpdate));
                sb.Append("Last remove: ");
                sb.AppendLine(UrlNotificationToString(response.UrlNotificationMetadata.LatestRemove));
            }

            return sb.ToString();
        }

        public static async Task<PublishUrlNotificationResponse> PublishRequest(string url, PublishType publishType, DateTime notifyTime)
        {
            var credentials = GetGoogleCredential().UnderlyingCredential;

            var googleIndexingApiClientService = new IndexingService(new Google.Apis.Services.BaseClientService.Initializer
            {
                HttpClientInitializer = credentials,
            });

            var requestBody = new UrlNotification
            {
                Url = url,
                Type = PublishTypeWords[(int)publishType],
                NotifyTime = notifyTime
            };

            var publishRequest = new UrlNotificationsResource.PublishRequest(googleIndexingApiClientService, requestBody);

            return await publishRequest.ExecuteAsync();
        }
    }
}
