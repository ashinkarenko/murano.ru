﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using MuranoWeb.Models;
using MuranoWeb.Models.Home;
using Newtonsoft.Json.Linq;

namespace MuranoWeb.Utils
{
    public static class JobPostingHelper
    {
        public static string BuildJobPostingData(VacancyWithOfficeModel vacancyData, DateTime updateDate, DateTime validThrough)
        {
            dynamic data = new JObject();
            data["@context"] = "https://schema.org/";
            data["@type"] = "JobPosting";
            data.title = vacancyData.Title;
            data.description = (vacancyData.Summary + vacancyData.Requirements).Replace("\"", "\\\"").Replace("\n", "");

            data.identifier = new JObject()
            {
                {"@type", "PropertyValue"},
                {"name", "MuranoSoft"},
                {"value", vacancyData.VacancyId.ToString()}
            };
            data.datePosted = updateDate.ToString("yyyy-MM-dd");
            data.validThrough = validThrough.ToString("yyyy-MM-dd"); 
            data.employmentType = "FULL_TIME";

            data.hiringOrganization = new JObject()
            {
                {"@type", "Organization"},
                {"name", "Murano Software"},
                {"sameAs", "https://muranosoft.ru"},
                {"logo", "https://muranosoft.ru/images/logo.png"},
                {"email", vacancyData.Office.Email}
            };

            dynamic jobLocation = new JObject()
            {
                {"@type", "Place"}
            };
            jobLocation.address = new JObject()
            {
                {"@type", "PostalAddress"},
                {"streetAddress", vacancyData.Office.Address},
                {"addressLocality", vacancyData.Office.City},
                {"postalCode", vacancyData.Office.Zip},
                {"addressCountry", vacancyData.Office.Country},
                {"addressRegion", vacancyData.Office.Region}
            };
            data.jobLocation = jobLocation;

            if (vacancyData.SalaryFrom != null || vacancyData.SalaryTo != null)
            {
                dynamic baseSalary = new JObject()
                {
                    {"@type", "MonetaryAmount"},
                    {"currency", vacancyData.Currency}
                };
                dynamic value = new JObject()
                {
                    {"@type", "QuantitativeValue"},
                    {"unitText", "MONTH"}
                };
                if (vacancyData.SalaryFrom != null)
                {
                    value.minValue = vacancyData.SalaryFrom.Value;
                }

                if (vacancyData.SalaryTo != null)
                {
                    value.maxValue = vacancyData.SalaryTo.Value;
                }

                baseSalary.value = value;
                data.baseSalary = baseSalary;
            }

            return data.ToString();
        }
    }
}
