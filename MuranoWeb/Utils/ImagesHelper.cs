﻿using MuranoWeb.Data.Entity;
using NLog;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats.Jpeg;
using Image = SixLabors.ImageSharp.Image;
using SixLabors.ImageSharp.Processing;
namespace MuranoWeb.Utils
{
    public class ImagesHelper
    {
        public const int ImagesLimit = 15;

        private const string PhotosPath = "images/photo";
        private const string PreviewPath = "preview";
        private const string IconsPath = "images/icons";
        private const int PreviewWidth = 500;

        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public static string GetImagesDirectory(string contentRootPath, Location location)
        {
            string directory = $"{contentRootPath}/{PhotosPath}/{location.ToString()}";
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            return directory;
        }

        public static string GetIconsDirectory(string contentRootPath)
        {
            string directory = $"{contentRootPath}/{IconsPath}";
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            return directory;
        }


        public static string GetImagesPreviewDirectory(string contentRootPath, Location location)
        {
            string directory = $"{contentRootPath}/{PhotosPath}/{location.ToString().ToLower()}/{PreviewPath}";
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            return directory;
        }

        public static string GetImageUrl(string fileName, Location location)
        {
            return $"/{PhotosPath}/{location.ToString().ToLower()}/{fileName}";
        }

        public static string GetIconUrl(string fileName)
        {
            return $"/{IconsPath}/{fileName}";
        }

        public static string GetIconUrl(VacancyIcon icon)
        {
            return $"/{IconsPath}/{icon.Name}";
        }

        public static string GetImagePreviewUrl(string fileName, Location location)
        {
            return $"/{PhotosPath}/{location.ToString()}/{PreviewPath}/{fileName}";
        }

        public static string MakeUniqueFileName(string directory, string fileName)
        {
            var name = System.IO.Path.GetFileNameWithoutExtension(fileName);
            var ext = System.IO.Path.GetExtension(fileName);
            var index = 1;
            var path = System.IO.Path.Combine(directory, fileName);
            while (File.Exists(path))
            {
                fileName = $"{name}({index}){ext}";
                path = System.IO.Path.Combine(directory, fileName);
                index++;
            }
            return fileName;
        }

        public static async void ResizePhoto(Stream stream, string filePath, string imageFileName, long maxFileSize)
        {
            var srcSize = stream.Length;
            stream.Position = 0;

            if (srcSize <= maxFileSize)
            {
                await using var fs = new FileStream(Path.Combine(filePath, imageFileName), FileMode.Create);
                await stream.CopyToAsync(fs);
                fs.Flush();
            }
            else
            {
                var image = await SixLabors.ImageSharp.Image.LoadAsync(stream);
                try
                {
                    var aspect = System.Math.Floor(System.Math.Sqrt(srcSize / (double)maxFileSize));
                    await ResizeAndSave(image, Path.Combine(filePath, imageFileName), (int)(image.Width / aspect), (int)(image.Height / aspect));
                }
                catch (System.Exception ex)
                {
                    var trace = ex.StackTrace;
                }
                finally
                {
                    image.Dispose();
                }
            }
        }


        public static async Task ResizeAndSave(Image image, string pathToSave, int newWidth, int newHeight)
        {
            image.Mutate(x => x.Resize(newWidth, newHeight));
            await image.SaveAsync(pathToSave, new JpegEncoder());
        }

        public static async Task<string> GeneratePreview(Stream stream, string contentRootPath, string imageFileName, Location location)
        {
            var srcDirectory = GetImagesDirectory(contentRootPath, location);
            var previewDirectory = GetImagesPreviewDirectory(contentRootPath, location);
            var previewImageFileName = MakeUniqueFileName(previewDirectory, imageFileName);
            
            stream.Position = 0;
            var image = await Image.LoadAsync(stream);
            try
            {
                if (image.Width <= PreviewWidth)
                {
                    //await using var fs = new FileStream(Path.Combine(previewDirectory, previewImageFileName), FileMode.Create);

                    //await stream.CopyToAsync(fs);
                    //await fs.FlushAsync();
                    await image.SaveAsync(previewImageFileName);
                }
                else
                {
                    var height = (int)(image.Height * PreviewWidth / (double)image.Width);
                    await ResizeAndSave(image, System.IO.Path.Combine(previewDirectory, previewImageFileName), PreviewWidth, height);
                }
            }
            finally
            {
                image.Dispose();
            }
            return previewImageFileName;
        }
    }
}
