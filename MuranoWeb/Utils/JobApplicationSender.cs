﻿using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using MuranoWeb.Config;
using MuranoWeb.Data.Entity;
using NLog;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MuranoWeb.Utils
{
    public class JobApplicationSender : IJobApplicationSender
    {
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly EmailingOptions _options;

        public JobApplicationSender(IOptions<EmailingOptions> options)
        {
            _options = options.Value;
        }


        public async Task Send(JobApplication jobApplication)
        {
            try
            {
                _logger.Info($"Sending application from {jobApplication.FirstName} {jobApplication.LastName}");
                using var client = new SmtpClient();
                await client.ConnectAsync(_options.Server, _options.Port, false).ConfigureAwait(false);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                // Note: only needed if the SMTP server requires authentication
                await client.AuthenticateAsync(_options.UserName, _options.Password).ConfigureAwait(false);

                foreach (var address in _options.GetAddress(jobApplication.Location))
                {
                    try
                    {
                        MimeMessage message = getMessage(jobApplication, address);
                        await client.SendAsync(message).ConfigureAwait(false);
                    }
                    catch (Exception e)
                    {
                        _logger.Log(LogLevel.Error, e);
                    }
                }

                await client.DisconnectAsync(true);
            }
            catch (Exception e)
            {
                _logger.Log(LogLevel.Error, e);
            }

        }

        private MimeMessage getMessage(JobApplication jobApplication, string messageRecipient)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress($"muranosoft.ru on behalf of {jobApplication.FirstName} {jobApplication.LastName}<{jobApplication.Email}>", _options.DefaultEmailAddress));
            //message.To.Add(new MailboxAddress(_options.GetAddress(jobApplication.Location)));
            message.To.Add(new MailboxAddress("", messageRecipient));
            message.Subject = _options.MessageSubject;
            var builder = new BodyBuilder
            {
                TextBody = ($"From: {jobApplication.FirstName} {jobApplication.LastName}\nEmail: {jobApplication.Email}\nLocation: {jobApplication.Location}\nVacancy: {(jobApplication.Vacancy == null ? "N/A" : jobApplication.Vacancy.Title)}\nComments: {jobApplication.Comment}")
            };
            if (jobApplication.FileContent != null && jobApplication.FileContent.Any())
            {

                var attachment = builder.Attachments.Add(jobApplication.FileName, new MemoryStream(jobApplication.FileContent));

                Parameter param;

                if (attachment.ContentDisposition.Parameters.TryGetValue("filename", out param))
                    param.EncodingMethod = ParameterEncodingMethod.Rfc2047;
            }
            message.Body = builder.ToMessageBody();

            return message;
        }
    }
}