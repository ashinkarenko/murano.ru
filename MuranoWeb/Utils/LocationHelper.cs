﻿using MuranoWeb.Data.Entity;

namespace MuranoWeb.Utils
{
    public static class LocationHelper
    {
        public static string GetLocationNameRus(Location location)
        {
            switch (location)
            {
                case Location.SaintPetersburg:
                    return "Санкт-Петербург";
                case Location.Voronezh:
                    return "Воронеж";
                case Location.Kharkov:
                    return "Харьков";
                case Location.Tashkent:
                    return "Ташкент";
                default:
                    return "Unknown";
            }
        }

        public static string GetLocationNameRusOf(Location location)
        {
            return GetLocationNameRus(location) + "е";
        }
    }
}
