﻿using MuranoWeb.Data.Entity;
using System.Threading.Tasks;

namespace MuranoWeb.Utils
{
    public interface IJobApplicationSender
    {
        Task Send(JobApplication jobApplication);
    }
}