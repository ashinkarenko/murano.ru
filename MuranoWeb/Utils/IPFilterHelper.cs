﻿using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MuranoWeb.Utils
{
    public static class IPFilterHelper
    {
        public static int BanInterval = 60; // Minutes
        public static int ShortRunInterval = 60; // Seconds
        public static int MaxShortRunsToBan = 2;
        public static int MaxShortRunsToFullBan = 6;
        public static async Task<UserStatus> CheckStatus(string IPAddress, IRepository<IpStatistic> _statisticRepository)
        {
            var now = DateTime.Now;
            if (string.IsNullOrEmpty(IPAddress))
                return UserStatus.Ok;

            var iprecords = _statisticRepository.GetAll().Where(x => x.ClientIp == IPAddress);
            IpStatistic item = null;
            if (iprecords != null && iprecords.Count() > 0)
                item = iprecords.ToArray()[0];

            if (item == null)
            {
                item = new IpStatistic()
                {
                    ClientIp = IPAddress,
                    LastSubmitAt = now,
                    Status = UserStatus.Ok,
                    ShortRunStatistic = 0,
                    BansCount = 0,
                    BannedAt = null
                };
                await _statisticRepository.Add(item);
                return item.Status;
            }

            // Unchanging statuses
            if (item.Status == UserStatus.Banned || item.Status == UserStatus.AlwaysActive)
                return item.Status;

            // check if we are in a short run
            if (now.Subtract(item.LastSubmitAt).TotalSeconds < ShortRunInterval)
            {
                item.ShortRunStatistic++;
                item.LastSubmitAt = now;

                if (item.ShortRunStatistic > MaxShortRunsToFullBan)
                {
                    item.Status = UserStatus.Banned;
                    item.BannedAt = now;
                }
                else if (item.ShortRunStatistic > MaxShortRunsToBan)
                {
                    item.Status = UserStatus.TemporaryBanned;
                    item.BannedAt = now;
                }

                await _statisticRepository.Update(item);
                return item.Status;
            }

            // if used was temporary banned and as we are not in short run - unban
            if (item.Status == UserStatus.TemporaryBanned)
            {
                if (now.Subtract(item.BannedAt.Value).TotalMinutes > BanInterval)
                {
                    item.BannedAt = null;
                    item.Status = UserStatus.Ok;
                    await _statisticRepository.Update(item);
                }
            }

            return item.Status;
        }
    }
}
