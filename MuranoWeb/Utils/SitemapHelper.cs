﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MuranoWeb.Utils
{
    public class SitemapHelper
    {
        public enum PageAction { Add, Delete }

        private SitemapHelper()
        {
        }

        private XDocument _doc;
        private XElement CreateElement(string name, string value)
        {
            return new XElement(_doc.Root.Name.Namespace + name, value);
        }
        public static SitemapHelper Load(string path)
        {
            return new SitemapHelper()
            {
                _doc = XDocument.Load(path)
            };
        }

        public void Save(string path)
        {
            _doc.Save(path);
        }

        public async Task SaveAsync(string path)
        {
            await using var stream = new FileStream(path, FileMode.Create);
            await _doc.SaveAsync(stream, SaveOptions.None, CancellationToken.None);
        }

        private XElement FindUrl(string pageUrl)
        {
            return _doc.Root.Descendants().FirstOrDefault(u => u.Name.LocalName == "loc" && u.Value == pageUrl)?.Parent;
        }

        private void AddOrUpdate(XElement element, string childName, string newValue)
        {
            var child = element.Elements().FirstOrDefault(u => u.Name.LocalName == childName);
            if (child == null)
            {
                element.Add(CreateElement(childName, newValue));
            }
            else
            {
                child.Value = newValue;
            }
        }
        private void UpdateUrl(XElement url, string pageUrl, DateTime lastMod, string priority)
        {
            AddOrUpdate(url, "loc", pageUrl);
            AddOrUpdate(url, "lastmod", lastMod.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss+00:00"));
            AddOrUpdate(url, "priority", priority);
        }

        public void UpdatePageRecord(string pageUrl, PageAction action)
        {
            if (action == PageAction.Add)
            {
                AddOrUpdatePage(pageUrl);
            }
            else
            {
                RemovePage(pageUrl);
            }
        }

        public void AddOrUpdatePage(string pageUrl, string priority = "1.00")
        {
            var existingUrl = FindUrl(pageUrl);
            if (existingUrl == null)
            {
                existingUrl = new XElement(_doc.Root.Name.Namespace + "url");
                _doc.Root.Add(existingUrl);
            }
            UpdateUrl(existingUrl, pageUrl, DateTime.Now, priority);
        }

        public void RemovePage(string pageUrl)
        {
            var existingUrl = FindUrl(pageUrl);
            existingUrl?.Remove();
        }
    }
}
