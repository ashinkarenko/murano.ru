﻿using System;
using System.IO;

namespace MuranoWeb.Utils
{
    public static class LocalizedSitemap
    {
        public enum SitemapLocale { Ru, Ua };
        private static string _commonSitemapFile = "sitemap_ru.xml";
        private static string _uaSitemapFile = "sitemap_ua.xml";
        private static string _commonTemplate = "muranosoft.ru";
        private static string _uaTemplate = "muranosoft.com.ua";
        private static string _uaHostTemplate = ".com.ua";
        public static string GetSitemapFileName(SitemapLocale locale)
        {
            switch (locale)
            {
                case SitemapLocale.Ua:
                    return _uaSitemapFile;
                default:
                    return _commonSitemapFile;
            }
        }

        private static string GetSitemapTemplate(SitemapLocale locale)
        {
            switch (locale)
            {
                case SitemapLocale.Ua:
                    return _uaTemplate;
                default:
                    return _commonTemplate;
            }
        }

        public static SitemapLocale GetLocaleFromHost (string host)
        {
            if (host?.Contains(_uaHostTemplate) == true)
            {
                return SitemapLocale.Ua;
            }

            return SitemapLocale.Ru;
        }

        private static void UpdateSitemap(string rootFolder, SitemapLocale src, SitemapLocale dst)
        {
            try { 
                var srcSitemap = File.ReadAllText(Path.Combine(rootFolder, GetSitemapFileName(src)));
                File.WriteAllText(Path.Combine(rootFolder, GetSitemapFileName(dst)),
                    srcSitemap.Replace(GetSitemapTemplate(src), GetSitemapTemplate(dst)));
            }
            catch { }
        }

        public static void UpdateSitemaps(string rootFolder, SitemapLocale srcLocale)
        {
            foreach (SitemapLocale loc in Enum.GetValues(typeof(SitemapLocale)))
            {
                if (loc != srcLocale)
                {
                    UpdateSitemap(rootFolder, srcLocale, loc);
                }
            }
        }
    }
}
