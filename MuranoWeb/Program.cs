﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using NLog.Web;
using System;
namespace MuranoWeb
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //NLog.LogManager.Configuration.Variables["configDir"] = Environment.CurrentDirectory;
            NLog.LogManager.ThrowExceptions = true;
            NLog.LogManager.ThrowConfigExceptions = true;
            var logger = NLog.Web.NLogBuilder.ConfigureNLog("NLog.config").GetCurrentClassLogger();
            NLog.Logger log = NLog.LogManager.GetLogger("any");

            try
            {
                logger.Debug("init app");
                BuildWebHost(args).Run();
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Application terminated");
                throw;
            }
            finally
            {
                NLog.LogManager.Shutdown();
            }
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>().
            ConfigureLogging(logging =>
            {
                logging.ClearProviders();
                logging.SetMinimumLevel(LogLevel.Information);
            })
            //.UseIIS()
            .UseKestrel(options=>options.AddServerHeader=false)
            .UseNLog()
            .Build();
    }
}
