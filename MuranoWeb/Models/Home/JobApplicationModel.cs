﻿using Microsoft.AspNetCore.Http;
using MuranoWeb.Data.Entity;

namespace MuranoWeb.Models.Home
{
    public class JobApplicationModel
    {
        public JobApplication Application { get; set; }
        public IFormFile CV { get; set; }
    }
}
