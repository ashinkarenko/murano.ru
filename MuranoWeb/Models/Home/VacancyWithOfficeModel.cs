﻿using MuranoWeb.Data.Entity;

namespace MuranoWeb.Models.Home
{
    public class VacancyWithOfficeModel : VacancyModel
    {
        public Office Office { get; set; }
    }
}