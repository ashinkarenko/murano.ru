﻿using MuranoWeb.Data.Entity;
using System.Collections.Generic;

namespace MuranoWeb.Models.Home
{
    public class OfficeModel
    {
        public IEnumerable<VacancyModel> Vacancies { get; set; }
        public Quote Quote { get; set; }
        public Location Location { get; set; }
        public string LocationRus { get; set; }
        public Office OfficeInfo { get; set; }
    }
}