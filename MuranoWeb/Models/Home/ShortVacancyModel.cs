﻿namespace MuranoWeb.Models.Home
{
    public class ShortVacancyModel
    {
        public int VacancyId { get; set; }
        public string Title { get; set; }
        public string Summary { get; set; }
        public string IconUrl { get; set; }
        public int ExternalId { get; set; }
    }
}