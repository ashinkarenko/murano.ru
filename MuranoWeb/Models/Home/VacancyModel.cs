﻿using MuranoWeb.Data.Entity;

namespace MuranoWeb.Models.Home
{
    public class VacancyModel : Vacancy
    {
        public string IconUrl { get; set; }
        public VacancySharingModel SharingInfo { get; set; }
    }
}
