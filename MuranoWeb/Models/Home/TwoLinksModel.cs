﻿namespace MuranoWeb.Models.Home
{
    public class TwoLinksModel
    {
        public string FullImage { get; set; }
        public string PreviewImage { get; set; }
    }
}
