﻿namespace MuranoWeb.Models.Home
{
    public class VacancySharingModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string TitleEncoded { get; set; }
        public string Url { get; set; }
        public string UrlEncoded { get; set; }
    }
}
