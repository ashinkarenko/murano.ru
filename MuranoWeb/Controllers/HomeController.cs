﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MuranoWeb.Config;
using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Repositories;
using MuranoWeb.Models.Home;
using MuranoWeb.Utils;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using MuranoWeb.Middleware;
namespace MuranoWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IRepository<JobApplication> _jobApplicationsRepository;
        private readonly IRepository<Vacancy> _vacancyRepository;
        private readonly IRepository<Quote> _quotesRepository;
        private readonly IRepository<Image> _imagesRepository;
        private readonly IRepository<IpStatistic> _statisticRepository;
        private readonly IRepository<VacancyLog> _logRepository;
        private readonly IRepository<Office> _officeRepository;
        private readonly JobApplicationOptions _applicationOptions;
        private readonly IWebHostEnvironment _appEnvironment;
        private readonly IJobApplicationSender _applicationSender;

        private readonly ILogger<HomeController> _logger;

        public HomeController(IRepository<JobApplication> jobApplicationsRepository,
            IRepository<Vacancy> vacancyRepository,
            IRepository<Quote> quotesRepository,
            IRepository<Image> imagesRepository,
            IRepository<IpStatistic> statisticRepository,
            IRepository<VacancyLog> logRepository,
            IRepository<Office> officeRepository,
            IJobApplicationSender applicationSender,
            IOptions<JobApplicationOptions> applicationOptions,
            IWebHostEnvironment appEnvironment,
            ILogger<HomeController> logger)
        {
            _jobApplicationsRepository = jobApplicationsRepository;
            _vacancyRepository = vacancyRepository;
            _quotesRepository = quotesRepository;
            _imagesRepository = imagesRepository;
            _applicationSender = applicationSender;
            _logRepository = logRepository;
            _officeRepository = officeRepository;
            _statisticRepository = statisticRepository;
            _applicationOptions = applicationOptions.Value;
            _appEnvironment = appEnvironment;
            _logger = logger;
        }

        [CountryToViewBag]
        public IActionResult Index()
        {
            _logger.LogInformation("Index");
            ViewBag.Canonical = "https://muranosoft.ru";
            var server = GetServerUrl(HttpContext);
            ViewBag.OgImageUrl = new Uri(server, "/images/cover-preload.jpg").ToString();
            ViewBag.OgImageWidth = "1920";

            return View();
        }

        [CountryToViewBag]
        public IActionResult Archived()
        {
            return View();
        }

        [CountryToViewBag]
        public IActionResult Error()
        {
            return View();
        }

        private static Uri GetServerUrl(HttpContext context)
        {
            return new Uri(new Uri(context.Request.GetDisplayUrl()).GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped));
        }

        private static VacancySharingModel BuildVacancySharingModel(HttpContext context, Vacancy v)
        {
            var server = GetServerUrl(context);
            var sharingModel = new VacancySharingModel()
            {
                Id = v.VacancyId,
                Title = $"Murano Software, Вакансия {v.Title} в {LocationHelper.GetLocationNameRusOf(v.Location)}",
                Url = new Uri(server, $"/vacancy/{v.ExternalId}").ToString()
            };

            sharingModel.UrlEncoded = HttpUtility.UrlEncode(sharingModel.Url);
            sharingModel.TitleEncoded = HttpUtility.UrlEncode(sharingModel.Title);
            return sharingModel;
        }

        static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);
        [CountryToViewBag]
        [HttpGet]
        public IActionResult Vacancy(int id)
        {
            var vacancy = _vacancyRepository.GetAll().Include(v => v.Icon).FirstOrDefault(v => v.ExternalId == id);

            if (vacancy == null)
            {
                return NotFound();
            }
            var model = new VacancyWithOfficeModel()
            {
                VacancyId = vacancy.VacancyId,
                Description = vacancy.Description,
                Location = vacancy.Location,
                Order = vacancy.Order,
                Requirements = vacancy.Requirements,
                Summary = vacancy.Summary,
                Title = vacancy.Title,
                IconUrl = ImagesHelper.GetIconUrl(vacancy.Icon),
                SharingInfo = BuildVacancySharingModel(HttpContext, vacancy),
                SalaryFrom = vacancy.SalaryFrom,
                SalaryTo = vacancy.SalaryTo,
                Currency = vacancy.Currency
            };
            var lastLogRecord = _logRepository
                .GetAll()
                .Where(v => v.VacancyId == vacancy.VacancyId)
                .OrderByDescending(vl => vl.Date)
                .First();

            if (lastLogRecord.Action == VacancyAction.Archive)
            {
                return new StatusCodeResult(StatusCodes.Status410Gone);
            }

            model.Office = _officeRepository.GetById((int)model.Location);
            model.Office.Address = _htmlRegex.Replace(model.Office.Address, string.Empty);
            var server = GetServerUrl(HttpContext);
            
            model.Office.OfficeImage = new  Uri(server, model.Office.OfficeImage).ToString();
            ViewBag.Canonical = HttpContext.Request.GetDisplayUrl()?.ToLower();
            ViewBag.JobPosting =
                JobPostingHelper.BuildJobPostingData(model, lastLogRecord.Date, lastLogRecord.Date.AddMonths(1));
            return View(model);
        }


        [CountryToViewBag]
        [HttpGet]
        public IActionResult Vacancies()
        {
            var vacancies = _vacancyRepository.GetAll()
                .Include(v => v.Icon)
                .Where(v => !v.InArchive)
                .OrderBy(v => v.Location)
                .ThenBy(v => v.Order)
                .Select(v => new ShortVacancyModel()
                {
                    VacancyId = v.VacancyId,
                    Title = $"{v.Title} ({LocationHelper.GetLocationNameRus(v.Location)})",
                    IconUrl = ImagesHelper.GetIconUrl(v.Icon),
                    Summary = v.Summary,
                    ExternalId = v.ExternalId
                }).ToList();
            var defaultOffice = _officeRepository.GetById((int) Location.SaintPetersburg);
            var server = GetServerUrl(HttpContext);
            ViewBag.OgImageUrl = new Uri(server, defaultOffice.OfficeImage);
            ViewBag.OgImageWidth = defaultOffice.OfficeImageWidth;
            return View(vacancies);
        }

        [CountryToViewBag]
        [HttpGet]
        public IActionResult Pda_ua()
        {
            return View();
        }

        [CountryToViewBag]
        [HttpGet]
        public IActionResult Pda_uz()
        {
            return View();
        }

        [CountryToViewBag]
        [HttpGet]
        public IActionResult Pda(Location location = Location.SaintPetersburg)
        {
            var description = "Соглашение об использовании персональных данных";
            switch (location)
            {
                case Location.Kharkov:
                    ViewBag.Description = description + ", Украина";
                    return View("pda_ua");
                case Location.Tashkent:
                    ViewBag.Description = description + ", Узбекистан";
                    return View("pda_uz");
                default:
                    ViewBag.Description = description + ", Российская Федерация";
                    return View();
            }

        }

        [CountryToViewBag]
        public IActionResult Office(Location location)
        {
            var path = HttpContext.Request.GetDisplayUrl();
            var pathCanonical = path.ToLower();

            if (path != pathCanonical)
            {
                return RedirectPermanent(url: pathCanonical);
            }

            try
            {
                if (Enum.IsDefined(typeof(Location), location))
                {
                    ViewBag.GalleryLinksList = _imagesRepository.GetAll().Where(x => x.Show && x.Location == location).OrderBy(x => x.SortOrder)
                       .Take(ImagesHelper.ImagesLimit).Select(x => new TwoLinksModel() { FullImage = ImagesHelper.GetImageUrl(x.Name, location), PreviewImage = ImagesHelper.GetImagePreviewUrl(x.PreviewName, location) });

                    var vacancies = _vacancyRepository.GetAll()
                        .Where(x => (x.Location & location) == location && !x.InArchive)
                        .OrderBy(x => x.Order)
                        .Select(x => new VacancyModel()
                        {
                            VacancyId = x.VacancyId,
                            Description = x.Description,
                            Location = x.Location,
                            Order = x.Order,
                            Requirements = x.Requirements,
                            Summary = x.Summary,
                            Title = x.Title,
                            IconUrl = ImagesHelper.GetIconUrl(x.Icon),
                            ExternalId = x.ExternalId
                        }).ToArray();
                    foreach (var vac in vacancies)
                    {
                        vac.SharingInfo = BuildVacancySharingModel(HttpContext, vac);
                    }
                    var quote = _quotesRepository.GetAll().OrderBy(x => Guid.NewGuid()).FirstOrDefault();
                    var office = _officeRepository.GetById((int)location);
                    var server = GetServerUrl(HttpContext);
                    office.OfficeImage = new Uri(server, office.OfficeImage).ToString();
                    ViewBag.Canonical = pathCanonical;
                    return View($"Offices/{location}", new OfficeModel
                    {
                        Vacancies = vacancies,
                        Quote = quote,
                        Location = location,
                        LocationRus = LocationHelper.GetLocationNameRus(location),
                        OfficeInfo = office
                    });
                }
                else
                {
                    return RedirectToAction("office", new { location = Location.SaintPetersburg.ToString().ToLower() });
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error occured preparing office info");
                return RedirectToAction("index", "home");
            }
        }

        [HttpGet]
        public IActionResult Sitemap()
        {
            var sitemapLocale = LocalizedSitemap.GetLocaleFromHost(GetServerUrl(HttpContext)?.Host);
            var sitemapFile = LocalizedSitemap.GetSitemapFileName(sitemapLocale);
            return PhysicalFile(Path.Combine(_appEnvironment.ContentRootPath, $"wwwroot/{sitemapFile}"), "text/xml", "sitemap.xml");
        }


        [ResponseCache(VaryByHeader = "User-Agent", Duration = 31536000)]
        public IActionResult StaticContent(string resource)
        {
            switch (resource)
            {
                case "sharecss":
                    return PhysicalFile(Path.Combine(_appEnvironment.ContentRootPath, "wwwroot/assets/share.css"), "text/css", "app.css");
                case "sharejs":
                    return PhysicalFile(Path.Combine(_appEnvironment.ContentRootPath, "wwwroot/assets/share.js"), "application/javascript", "share.js");
                case "css":
                    return PhysicalFile(Path.Combine(_appEnvironment.ContentRootPath, "wwwroot/assets/app.css"), "text/css", "app.css");
                case "app":
                    return PhysicalFile(Path.Combine(_appEnvironment.ContentRootPath, "wwwroot/assets/app.js"), "application/javascript", "app.js");
                default:
                    return NotFound();
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> PostApplication(Location location, int? vacancyId, [FromForm] JobApplicationModel model)
        {
            try
            {
                var clientIp = Request.HttpContext.Connection.RemoteIpAddress;
                if (clientIp != null)
                {
                    _logger.LogDebug("Client ip: " + clientIp);
                    var status = await IPFilterHelper.CheckStatus(clientIp.ToString(), _statisticRepository);
                    if (status == UserStatus.Banned || status == UserStatus.TemporaryBanned)
                        return new ForbidResult();
                }
                if (ModelState.IsValid)
                {
                    if (model.CV != null)
                    {
                        if (!ValidateFile(model.CV))
                            return new ForbidResult();
                    }

                    var jobApplication = new JobApplication
                    {
                        FirstName = model.Application.FirstName,
                        LastName = model.Application.LastName,
                        Email = model.Application.Email,
                        Comment = model.Application.Comment,
                        Location = location,
                        VacancyId = vacancyId,
                        FileName = model.CV?.FileName,
                        FileContent = await GetFileContent(model.CV),
                        CreatedOn = DateTime.Now
                    };
                    await _jobApplicationsRepository.Add(jobApplication);
                    if (vacancyId.HasValue)
                        jobApplication.Vacancy = await _vacancyRepository.GetByIdAsync(vacancyId.Value);
                    await _applicationSender.Send(jobApplication);
                    return new StatusCodeResult(200);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "PostApplication");
            }
            return new StatusCodeResult(500);
        }

        private bool ValidateFile(IFormFile file)
        {
            if (!string.IsNullOrEmpty(file.FileName))
            {
                var extension = Path.GetExtension(file.FileName);
                if (!string.IsNullOrEmpty(extension))
                {
                    return _applicationOptions.IsExtensionValid(extension);
                }
                else
                    return true;
            }
            return true;
        }
        private async Task<byte[]> GetFileContent(IFormFile file)
        {
            _logger.LogInformation("getFileContent");
            if (file != null)
            {
                await using var memoryStream = new MemoryStream();
                await file.CopyToAsync(memoryStream).ConfigureAwait(false);
                return memoryStream.ToArray();
            }
            return null;
        }
    }
}