import {Component, OnInit, EventEmitter, Input, Output} from '@angular/core';
import {OfficeLocationsService} from '../services/office-locations.service';
import {OfficeLocation} from '../models/office-location';
import { CookieService } from 'ngx-cookie-service';
import { PaginatorComponent } from '../paginator/paginator.component';
@Component({
  selector: 'app-location-selector',
  templateUrl: './location-selector.component.html',
  styleUrls: ['./location-selector.component.css']
})

export class LocationSelectorComponent implements OnInit {
  
  @Input()
  currentLocation: OfficeLocation;

  @Input()
  defaultLabel: string;

  @Input()
  firstAsDefault: boolean;

  @Input()
  showAll : boolean;

  @Output()
  locationSelect: EventEmitter<OfficeLocation> = new EventEmitter<OfficeLocation>();
  
  locations: OfficeLocation[];
  cookieName: string = "Location";
  
  constructor(private service: OfficeLocationsService, private cookie : CookieService) { 
  }

  ngOnInit() : void {
    this.service.getLocations().then(v => {
      this.locations = v.slice();
      if (this.defaultLabel) {
        this.locations.unshift({
          code: null,
          name: this.defaultLabel
        } as OfficeLocation);
      }
      if (this.firstAsDefault) {
        var location: OfficeLocation = null;
        if (this.cookie.check(this.cookieName)) {
          var code = this.cookie.get(this.cookieName);
          location = this.locationByCode(+code);
        }
        if (location == null && !this.showAll) {
          location = this.locations[0];
        }
        this.locationChange(location);
      }
    });
  }

  locationByCode(code : number) : OfficeLocation {
    for (var i=0; i<this.locations.length; i++) {
      if (this.locations[i].code == code)
        return this.locations[i];
    }
    return null;
  }

  locationChange(location: OfficeLocation) {
    this.currentLocation = location;
    if (location !== null)
      this.cookie.set(this.cookieName, location.code.toString());
    this.locationSelect.emit(location);
  }

}
