import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';
import { JobApplicationService } from '../services/job-application.service';
import { JobApplications } from '../models/job-applications';
import { JobApplication } from '../models/job-application';
import { ActivatedRoute } from '@angular/router';
import { OfficeLocation } from '../models/office-location';
import { Paged} from '../abstract/paged';

@Component({
  selector: 'app-job-applications-list',
  templateUrl: './job-applications-list.component.html',
  styleUrls: ['./job-applications-list.component.css']
})

export class JobApplicationsListComponent extends Paged implements OnInit {
    applications: JobApplication[];
  curentLocation: OfficeLocation;

  constructor(private service: JobApplicationService, private route: ActivatedRoute) {
    super();
   }

  ngOnInit() {
    this.curentLocation = null;
    this.move(1);
  }


  move(destPage: number): void {
    if (this.canMoveTo(destPage)) {
      this.route.queryParams.subscribe(params => {
        const conditions: { [s: string]: string; } = {};
        const vacancyId = +params['vacancy'];
        conditions.page = destPage.toString();
        if (this.curentLocation !== null) conditions.location = this.curentLocation.name;
        if (vacancyId) {
          conditions.vacancyId = vacancyId.toString();
        }

        this.service.getPage(conditions).then(x => { this.applications = x.items; this.updatePagingInfo(x); });
        
      });
    }
  }

  openPage(destPage: any) {
    this.move(destPage);
  }
  locationSelect(location: OfficeLocation) {
    this.curentLocation = location;
    this.move(this.currentPage);
  }
  deleteById(id: number): void {
    this.service.removeById(id).then(x => this.move(this.currentPage));
  }

}
