import { keyframes } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { timer } from 'rxjs';
import { Office } from '../models/office';
import { OfficeLocation } from '../models/office-location';
import { OfficeService } from '../services/office.service';

@Component({
  selector: 'app-office-edit',
  templateUrl: './office-edit.component.html',
  styleUrls: ['./office-edit.component.css']
})

export class OfficeEditComponent implements OnInit {
  officeInfo: Office;
  location: OfficeLocation;
  message: string;
  msgShown: boolean;
  isMsgInfo: boolean;
  constructor(private service: OfficeService) { }

  ngOnInit(): void {
    this.message = null;
    this.msgShown = false;
    this.isMsgInfo = true;
  }

  locationSelect(location: OfficeLocation) {
    this.location = location;
    this.service.getById(this.location.code).then( response => this.officeInfo = response);
  }

  save(officeInfoForm: NgForm) {
    if (officeInfoForm.valid) {
      this.service.update(this.officeInfo.id, this.officeInfo)
      .then(r=>this.showMessage("Информация об офисе сохранена", true))
      .catch(r=>this.showMessage("Ошибка сохранения информации об офисе", false));
    }
  }

  showMessage(msg: string, isInfo: boolean) {
    document.getElementById('msg_text').classList.add('msg_text');
    this.message = msg;
    this.msgShown = true;
    this.isMsgInfo = isInfo;
    var tmr = timer(3000, 0);
    tmr.subscribe(t=>{
      this.message = null; this.msgShown=false;
      document.getElementById('msg_text').classList.remove('msg_text');
    });
  }
}
