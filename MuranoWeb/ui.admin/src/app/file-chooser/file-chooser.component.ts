import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-file-chooser',
  templateUrl: './file-chooser.component.html',
  styleUrls: ['./file-chooser.component.css']
})
export class FileChooserComponent implements OnInit {
  @Input()
  multiple: boolean = false;

  @Output()
  filesChoose: EventEmitter<FileList> = new EventEmitter<FileList>();

  constructor() {
  }

  ngOnInit() : void {
  }

  onChange(files: FileList) {
    this.filesChoose.emit(files);
  }

}
