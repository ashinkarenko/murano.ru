import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainNavigationComponent } from './main-navigation/main-navigation.component';
import { OfficeLocationsService } from './services/office-locations.service';
import { DataService } from './services/data.service';
import { IconsService } from './services/icons.service';
import { VacanciesListComponent } from './vacancies-list/vacancies-list.component';
import { ModalComponent } from './modal/modal.component';
import { LocationSelectorComponent } from './location-selector/location-selector.component';
import { HttpClientModule } from '@angular/common/http';
import { TinymceModule } from 'angular2-tinymce';
import { VacancyEditComponent } from './vacancy-edit/vacancy-edit.component';
import { FormsModule } from '@angular/forms';
import { PhotosListComponent } from './photos-list/photos-list.component';
import { FileChooserComponent } from './file-chooser/file-chooser.component';
import { IconsListComponent } from './icons-list/icons-list.component';
import { QuotesListComponent } from './quotes-list/quotes-list.component';
import { JobApplicationsListComponent } from './job-applications-list/job-applications-list.component';
import { JobApplicationService } from './services/job-application.service';
import { CookieService } from 'ngx-cookie-service';
import { UserService } from './services/user.service';
import { PaginatorComponent } from './paginator/paginator.component';
import { OfficeEditComponent } from './office-edit/office-edit.component';
@NgModule({
  declarations: [
    AppComponent,
    MainNavigationComponent,
    VacanciesListComponent,
    ModalComponent,
    LocationSelectorComponent,
    VacancyEditComponent,
    PhotosListComponent,
    FileChooserComponent,
    IconsListComponent,
    QuotesListComponent,
    JobApplicationsListComponent,
    PaginatorComponent,
    OfficeEditComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    
    TinymceModule.withConfig({
      menubar: "file edit format",
      menu: {
          file: { title: 'File', items: 'newdocument' },
          edit: { title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall' },
          format: { title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat' },
      },
      block_formats: 'Header=h4',
      style_formats: [
          { title: 'Header', block: 'h4'},
          { title: 'Bold text', inline: 'strong' }
      ],
      forced_root_block: 'p'
  }),
    FormsModule
  ],
  providers: [OfficeLocationsService, IconsService, JobApplicationService, CookieService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
