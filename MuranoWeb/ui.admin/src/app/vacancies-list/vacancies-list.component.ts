import { Component, OnInit } from '@angular/core';
import { VacanciesService } from '../services/vacancies.service';
import { Vacancy } from '../models/vacancy';
import { ActivatedRoute } from '@angular/router';
import { ModalComponent } from '../modal/modal.component';
import { VacancyLog } from '../models/vacancy-log';
import {OfficeLocation} from '../models/office-location';
import {Sortable} from '../decorators/sortable.decorator';
import { Paged } from '../abstract/paged';
@Component({
  selector: 'app-vacancies-list',
  templateUrl: './vacancies-list.component.html',
  styleUrls: ['./vacancies-list.component.css']
})
@Sortable('vacancies', 'onSort')
export class VacanciesListComponent extends Paged implements OnInit {
  vacancies: Vacancy[];
  logs: VacancyLog[];
  inArchive: boolean;
  location: OfficeLocation;
  searchString: string;
  constructor(private service: VacanciesService, private route: ActivatedRoute) {
    super();
   }

  ngOnInit() : void {
    this.inArchive = this.route.snapshot.data['inArchive'];
    this.searchString = "";
  }

  locationSelect(location: OfficeLocation) {
    this.location = location;
    this.loadVacancies();
  }

  loadVacancies() {
    //this.service.getAll({ location: this.location.code.toString(), archived: this.inArchive ? "true" : "false", search: this.searchString }).then(v => this.vacancies = v);
    this.move(1);
  }

  showLog(vacancy: Vacancy, modal: ModalComponent) {
    this.service.getLogs(vacancy.id).then(x => {
      this.logs = x;
      modal.show();
    });
  }

  onSort() {
    this.service.sortVacancies(this.location.code, this.vacancies).then(() => this.loadVacancies());
  }

  closeLogs() {
    this.logs = [];
  }

  search(filter: string) {
    if (this.searchString != filter) {
      this.searchString = filter;
      this.loadVacancies();
    }
  }

  searchValueChanged(filter: string) {
    if (this.searchString != filter && filter=='') {
      this.searchString = filter;
      this.loadVacancies();
    }
  }


  move(destPage: number): void {
    if (this.canMoveTo(destPage)) {
      this.route.queryParams.subscribe(params => {
        const conditions: { [s: string]: string; } = {};
        conditions.page = destPage.toString();
        if (this.location !== null) conditions.location = this.location.name;
        if (this.searchString != "") {
          conditions.search = this.searchString;
        }
        conditions.archived = this.inArchive ? "true" : "false";
        this.service.getPage(conditions).then(x => { this.vacancies = x.items; this.updatePagingInfo(x); });
        
      });
    }
  }

  openPage(destPage: any) {
    this.move(destPage);
  }

  formatDate(dateString: string) : string {
    let date = new Date(dateString);
    return date.getDate().toString() + "/"+date.getMonth().toString() + "/"+date.getFullYear().toString();
  }
}
