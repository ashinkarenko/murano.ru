import { HttpHeaders, HttpClient } from '@angular/common/http';

export class DataService<T> {
    private headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

    constructor(protected http: HttpClient, protected url: string) { }

    public getAll(conditions?: { [s: string]: string; }): Promise<T[]> {
        return this.http.get<T[]>(this.url, {params: conditions})
                        .toPromise()
                        .then(response => response)
                        .catch(this.handleError);
    }

    public getById(id: number): Promise<T> {
        return this.http.get<T>(`${this.url}/${id}`)
                        .toPromise()
                        .then(response => response)
                        .catch(this.handleError);
    }

    public add(item: T): Promise<T> {
        return this.http.post(this.url, JSON.stringify(item), {headers: this.headers})
                        .toPromise()
                        .then(response => response as T)
                        .catch(this.handleError);
    }

    public update(id: number, vacancy: T): Promise<T> {
        return this.http.put(`${this.url}/${id}`, JSON.stringify(vacancy), {headers: this.headers})
                        .toPromise()
                        .then(response => response as T)
                        .catch(this.handleError);
    }

    protected handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
