import { DataService } from './data.service';
import { Quote } from '../models/quote';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class QuoteService extends DataService<Quote> {
  constructor(http: HttpClient) {
      super(http, 'admin/api/quotes');
  }

  public delete(id: number): Promise<any> {
      return this.http.delete(`${this.url}/${id}`).toPromise().catch(super.handleError);
  }
}
