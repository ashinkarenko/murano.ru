import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OfficeLocation } from '../models/office-location';
import { DataService } from './data.service';
import { Observable } from 'rxjs';
//import 'rxjs/add/operator/toPromise';

@Injectable({
  providedIn: 'root'
})
export class OfficeLocationsService extends DataService<OfficeLocation> {
  private locations: OfficeLocation[];

  constructor(http: HttpClient) {
    super(http, 'admin/api/locations');
    this.locations = [];
  }

  public getLocations(): Promise<OfficeLocation[]> {
    if (this.locations.length > 0) {
      return Promise.resolve(this.locations);
    }

    return super.getAll().then(r => {
        this.locations = r;
        return this.locations;
    });
  }

  public getLocation(code: number): Promise<OfficeLocation> {
    return this.getLocations()
      .then(locations => locations.filter(v => v.code === code)[0])
      .catch(this.handleError);
  }
}


