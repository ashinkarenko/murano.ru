import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Office } from '../models/office';
import { DataService } from './data.service';

@Injectable({
  providedIn: 'root'
})

export class OfficeService extends DataService<Office> {
  constructor(protected http: HttpClient)  {
    super(http, 'admin/api/office');
   }
}