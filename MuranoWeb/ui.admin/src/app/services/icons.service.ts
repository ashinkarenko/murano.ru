import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { Icon } from '../models/icon';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class IconsService extends DataService<Icon> {
    private multipartHeaders: HttpHeaders = new HttpHeaders({'Content-Type': 'multipart/form-data; charset=UTF-8'});

    constructor(http: HttpClient) {
        super(http, 'admin/api/icons');
    }

    public delete(id: number): Promise<any> {
        return this.http.delete(`${this.url}/${id}`).toPromise().catch(super.handleError);
    }

    public uploadIcons(files: FileList): Promise<any> {
        return this.http.post( this.url, this.makeFormData(files), {headers: this.multipartHeaders} ).toPromise().catch(this.handleError);
    }

    public getIcons(): Promise<Icon[]> {
        return this.http.get<Icon[]>(this.url)
            .toPromise()
            .then(response => response)
            .catch(this.handleError);
    }

    public deleteIcon(icon: Icon) {
        return this.http.delete(`${this.url}/${icon.id}`)
            .toPromise()
            .catch(this.handleError);
    }

    public updateIcon(icon: Icon) {
        return this.update(icon.id, icon)
            .catch(this.handleError);
    }

    public replaceIcon(icon: Icon, files: FileList) {
        return this.http.post(`${this.url}/${icon.id}`, this.makeFormData(files), {headers: this.multipartHeaders})
            .toPromise()
            .catch(this.handleError);
    }

    private makeFormData(files: FileList): FormData {
        let formData: FormData = new FormData();
        for (let i: number = 0; i < files.length; i++) {
            formData.append('files', files[i], files[i].name);
        }
        return formData;
    }
}
