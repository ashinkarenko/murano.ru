﻿import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Vacancy } from '../models/vacancy';
//import { DataService } from './data.service';
import { PagingDataService } from './paging-data.service';
import { VacancyLog } from '../models/vacancy-log';
import { Vacancies } from '../models/vacancies';

//import 'rxjs/add/operator/toPromise';

@Injectable({
  providedIn: 'root'
})

@Injectable()
export class VacanciesService extends PagingDataService<Vacancies, Vacancy> {
    constructor(http: HttpClient) { super(http, 'admin/api/vacancies'); }

    public save(vacancy: Vacancy): Promise<Vacancy> {
        return vacancy.id ? super.update(vacancy.id, vacancy) : super.add(vacancy);
    }

    public toArchive(id: number): Promise<Vacancy> {
        return this.http.delete(`${this.url}/${id}`).toPromise()
            .then(response => response).catch(super.handleError);
    }

    public fromArchive(id: number): Promise<Vacancy> {
        return this.http.options(`${this.url}/${id}`).toPromise()
            .then(response => response as Vacancy).catch(super.handleError);
    }

    public getLogs(id: number): Promise<VacancyLog[]> {
        return this.http.get(`${this.url}/${id}/log`).toPromise()
            .then(response => response as VacancyLog[]).catch(super.handleError);
    }

    public sortVacancies(locationCode: number, vacancies: Vacancy[]) {
        return this.http.patch(`${this.url}/sort/${locationCode}`, vacancies.map(v => v.id)).toPromise()
            .catch(this.handleError);
    }
}
