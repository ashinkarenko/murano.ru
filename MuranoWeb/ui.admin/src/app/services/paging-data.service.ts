import { DataService } from './data.service';
import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PagingDataService<T1, T2> extends DataService<T2> {
  constructor(http: HttpClient, protected url: string) { super(http, url); }

  public getPage(conditions?: { [s: string]: string; }): Promise<T1> {
    return this.http.get<T1>(this.url, { params: conditions })
      .toPromise()
      .then(response => response)
      .catch(this.handleError);
  }

}