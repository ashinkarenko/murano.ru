import { TestBed } from '@angular/core/testing';

import { PagingDataService } from './paging-data.service';

describe('PagingDataService', () => {
  let service: PagingDataService<any>;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PagingDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
