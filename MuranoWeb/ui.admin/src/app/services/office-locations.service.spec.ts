import { TestBed } from '@angular/core/testing';

import { OfficeLocationsService } from './office-locations.service';

describe('OfficeLocationService', () => {
  let service: OfficeLocationsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfficeLocationsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
