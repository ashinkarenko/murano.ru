import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { JobApplications } from '../models/job-applications';
import { JobApplication } from '../models/job-application';
import { PagingDataService } from './paging-data.service';

@Injectable()
export class JobApplicationService extends PagingDataService<JobApplications, JobApplication> {
  constructor(http: HttpClient) { super(http, 'admin/api/applications'); }

  public removeById(id: number): Promise<void> {
    return this.http.get(`${this.url}/${id}/rmapp`).toPromise()
      .catch(this.handleError);
  }
}
