import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})


export class UserService {
    private headers: HttpHeaders = new HttpHeaders({'Content-Type': 'application/json'});
    public currentUser : string = null;
    constructor(protected http: HttpClient) { }

    public getCurrent(): Promise<string> {
      if (this.currentUser !== null) {
        return Promise.resolve(this.currentUser);
      }

        return this.http.get<string>('admin/api/user')
                        .toPromise()
                        .then(response => { this.currentUser = response; return this.currentUser})
                        .catch(this.handleError);
    }

    protected handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}

