import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Photo } from '../models/photo';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  private url = 'admin/api/images';  // URL to web api

  constructor(private http: HttpClient) {}

  public getPhotos(locationCode: number): Promise<Photo[]> {
    return this.http.get<Photo[]>(`${this.url}/${locationCode}`)
                    .toPromise()
                    .then(response => response)
                    .catch(this.handleError);
  }

  public uploadPhotos(locationCode: number, files: FileList): Promise<any> {
    return this.http.post(`${this.url}`, this.makeFormData(locationCode, files))
                    .toPromise()
                    .catch(this.handleError);
  }

  public toggleShow(locationCode: number, photo: Photo) {
    return this.http.patch(`${this.url}/${locationCode}/${photo.id}`, {})
                    .toPromise()
                    .catch(this.handleError);
  }

  public sortPhotos(locationCode: number, photos: Photo[]) {
    return this.http.patch(`${this.url}/sort/${locationCode}`, photos.map(v => v.id))
                    .toPromise()
                    .catch(this.handleError);
  }

  public deletePhoto(locationCode: number, photo: Photo) {
    return this.http.delete(`${this.url}/${photo.id}`)
                    .toPromise()
                    .catch(this.handleError);
  }

  private makeFormData(locationCode: number, files: FileList): FormData {
    let formData: FormData = new FormData();
    formData.append('location', locationCode.toString());
    for (let i: number = 0; i < files.length; i++) {
      formData.append('files', files[i], files[i].name);
    }
    return formData;
  }

  private handleError(error: any): Promise<any> {
    return Promise.reject(error.message || error);
  }
}
