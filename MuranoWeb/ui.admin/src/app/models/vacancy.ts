﻿import { OfficeLocation } from './office-location';
import { Icon } from './icon';
export class Vacancy {
  id: number;
  title: string;
  location: string;
  summary: string;
  description: string;
  requirements: string;
  inArchive: boolean;
  iconId: number;
  ext: number;
  archivedOn: Date;
  salaryFrom: number;
  salaryTo: number;
  currency: string;
  availableLocations: OfficeLocation[];
  availableIcons: Icon[];
  availableCurrencies: string[];
}
