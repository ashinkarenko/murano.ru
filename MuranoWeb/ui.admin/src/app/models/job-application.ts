export class JobApplication {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    comment: string;
    fileName: string;
    vacancyId?: number;
    vacancyName: string;
    location: number;
    locationName: string;
}
