import { PagingModel } from './paging-model';
export class PagingModelWithItems<T> extends PagingModel {
    items: T[];
    constructor() {
        super();
    }
}
