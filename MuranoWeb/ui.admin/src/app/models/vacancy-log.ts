export class VacancyLog {
    action: string;
    user: string;
    date: Date;
}
