export class Office {
  id: number;
  country: string;
  city: string;
  address: string;
  zip: string;
  phone: string;
  email: string;
  region: string;
  lat: number;
  long: number;
  officeimage: string;
}
