import { JobApplication } from './job-application';
import { PagingModelWithItems } from './paging-model-with-items';

export class JobApplications extends PagingModelWithItems<JobApplication>  {
  
}
