
export function Sortable(listProperty: string, sortingCallback: string): ClassDecorator {

  function DecoratorFactory(target: Function): void {

    target.prototype.startSorting = function startSorting(item: any) {  
      this.sortableItem = item;
    };

    target.prototype.stopSorting = function stopSorting() {
      this.sortableItem = null;
      this[sortingCallback]();
    };

    target.prototype.swapSortedItems = function swapItems(item: any, event: Event) {
      if (!this.sortableItem) {
        return;
      }
      event.preventDefault();
      let itemIndex: number = this[listProperty].indexOf(item);
      let sortableItemIndex: number = this[listProperty].indexOf(this.sortableItem);
      if (itemIndex === -1 || sortableItemIndex === -1) {
        return;
      }
      this[listProperty][sortableItemIndex]= item;
      this[listProperty][itemIndex]= this.sortableItem;
    };

  }

  return DecoratorFactory;

}
