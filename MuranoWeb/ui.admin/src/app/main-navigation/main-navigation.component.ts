import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-main-navigation',
  templateUrl: './main-navigation.component.html',
  styleUrls: ['./main-navigation.component.css']
})
export class MainNavigationComponent implements OnInit {
  public currentUser : string;

  constructor( private userService: UserService ) { }

  ngOnInit(): void {
    this.userService.getCurrent()
    .then(response => 
      { 
        this.currentUser = response 
      });
  }

}
