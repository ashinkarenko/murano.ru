import { Component, OnInit } from '@angular/core';
import { Quote } from '../models/quote';
import { QuoteService } from '../services/quote.service';

@Component({
  selector: 'app-quotes-list',
  templateUrl: './quotes-list.component.html',
  styleUrls: ['./quotes-list.component.css']
})
export class QuotesListComponent implements OnInit {
  quotes: QuoteModel[];
  constructor(private service: QuoteService) { }

  ngOnInit() : void {
    this.service.getAll().then(q => this.quotes = q.map(m => new QuoteModel(m, null)));
  }

  editQuote(quote: QuoteModel) {
    quote.editQuote = {id: quote.quote.id, author: quote.quote.author, text: quote.quote.text};
  }

  closeEdit(quote: QuoteModel) {
    quote.editQuote = null;
    if (quote.quote.id === 0) {
      this.quotes.splice(this.quotes.indexOf(quote), 1);
    }
  }

  addNew() {
    const newQuote = {id: 0, author: '', text: ''};
    this.quotes.push(new QuoteModel(newQuote, newQuote));
  }

  deleteQuote(quote: QuoteModel) {
    this.service.delete(quote.quote.id).then(x => this.quotes.splice(this.quotes.indexOf(quote), 1));
  }

  saveQuote(quote: QuoteModel) {
    if (quote.editQuote.author !== '' && quote.editQuote.text !== '') {
      const promise = quote.editQuote.id === 0
          ? this.service.add(quote.editQuote)
          : this.service.update(quote.editQuote.id, quote.editQuote);

      promise.then(x => {
        if (x) {
          quote.quote.id = x.id;
        }
        quote.quote.author = quote.editQuote.author;
        quote.quote.text = quote.editQuote.text;

        this.closeEdit(quote);
      });
    }
  }
}

class QuoteModel {
  constructor(public quote: Quote, public editQuote: Quote) {}
}
