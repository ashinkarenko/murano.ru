import { PagingModel } from '../models/paging-model';
export abstract class Paged {
    public pagesCount: number;
    public currentPage: number;
    
    constructor() {
        this.pagesCount = 1;
        this.currentPage = 1;
    }
    
    updatePagingInfo(model: PagingModel) {
        this.currentPage = model.currentPage;
        this.pagesCount = model.pagesCount;
    }

    hasPrevPage(): boolean {
        return this.currentPage > 1;
      }
    
      hasNextPage(): boolean {
        return this.currentPage < this.pagesCount;
      }
    
      isFirst(): boolean {
        return this.currentPage == 1;
      }
    
      isLast(): boolean {
        return this.pagesCount == this.currentPage;
      }
    
      adjustPageNum(destPage: number): number {
        if (destPage < 1)
          return 1;
        if (destPage > this.pagesCount)
          return this.pagesCount;
    
        return destPage;
      }
    
      canMoveTo(destPage: number): boolean {
        return (destPage >= 1 && destPage <= this.pagesCount);
      }
    
      abstract move(destPage: number): void;

      moveNext(): void {
        this.move(this.currentPage+1);
      }
    
      moveBack(): void {
        this.move(this.currentPage-1);
      }
      moveFirst(): void {
        this.move(1);
      }
      moveLast(): void {
        this.move(this.pagesCount);
      }
    
}
