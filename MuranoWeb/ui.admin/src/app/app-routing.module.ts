import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VacanciesListComponent } from './vacancies-list/vacancies-list.component';
import { VacancyEditComponent } from './vacancy-edit/vacancy-edit.component';
import { PhotosListComponent } from './photos-list/photos-list.component';
import { IconsListComponent } from './icons-list/icons-list.component';
import { QuotesListComponent } from './quotes-list/quotes-list.component';
import { JobApplicationsListComponent } from './job-applications-list/job-applications-list.component';
import { OfficeEditComponent } from './office-edit/office-edit.component';
const routes: Routes = [
  { path: 'vacancies/opened',      component: VacanciesListComponent, data: { inArchive: false, title: 'Opened Vacancies' } },
  { path: 'vacancies/archive',     component: VacanciesListComponent, data: { inArchive: true, title: 'Archived Vacancies' } },
  { path: 'vacancies/opened/:id',  component: VacancyEditComponent, data: { title: 'Edit Vacancy' } },
  { path: 'vacancies/:id',         component: VacancyEditComponent, data: { title: 'Edit Vacancy' } },
  { path: 'vacancies/archive/:id', component: VacancyEditComponent, data: { title: 'Edit Vacancy' } },
  { path: 'vacancies/new',         component: VacancyEditComponent, data: { title: 'Add Vacancy' } },
  { path: 'job-applications',      component: JobApplicationsListComponent, data: { title: 'Job Applications' } },  
  { path: 'photos',                component: PhotosListComponent, data: { title: 'Photos' } },
  { path: 'quotes',                component: QuotesListComponent, data: { title: 'Quotes' } },
  { path: 'icons',                 component: IconsListComponent, data: { title: 'Icons' } },
  { path: 'editoffice',             component: OfficeEditComponent, data: { title: 'Edit office' } },

  { path: '', redirectTo: 'vacancies/opened', pathMatch: 'full' },
  { path: 'vacancies', redirectTo: 'vacancies/opened', pathMatch: 'full' }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
