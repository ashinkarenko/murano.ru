import {Component, OnInit, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit {
  @Input()
  pagesCount: number;


  @Input()
  currentPage: number;

  @Output()
  openPage: EventEmitter<number> = new EventEmitter<number>();

  constructor() {
   }

  ngOnInit(): void {
    this.currentPage = 1;
    this.pagesCount = 1;
  }

  move(destPage: any): void {
    this.openPage.emit(destPage);
  }

}
