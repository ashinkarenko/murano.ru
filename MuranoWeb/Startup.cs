﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.WebEncoders;
using MuranoWeb.Config;
using MuranoWeb.Data;
using MuranoWeb.Data.Repositories;
using MuranoWeb.Filters;
using MuranoWeb.Models;
using MuranoWeb.Utils;
using System.Text.Encodings.Web;
using System.Text.Unicode;
using Microsoft.AspNetCore.ResponseCompression;

namespace MuranoWeb
{
    public class Startup
    {
        //protected ILoggerFactory _loggerFactory;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<IISServerOptions>(options =>
            {
                options.AutomaticAuthentication = false;
            });
            services.Configure<GzipCompressionProviderOptions>(options => options.Level = System.IO.Compression.CompressionLevel.Optimal);
            services.AddResponseCompression(options =>
            {
                options.EnableForHttps = true;
                options.Providers.Add<GzipCompressionProvider>();
                options.MimeTypes = new[]
                {
                    // Default
                    "text/plain",
                    "text/css",
                    "application/javascript",
                    "text/html",
                    "application/xml",
                    "text/xml",
                    "application/json",
                    "text/json",
                    // Custom
                    "image/svg+xml"
                };

            });

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection"))
            );

            services.AddIdentity<User, IdentityRole>(
                //options=> options.SignIn.RequireConfirmedAccount = true
                ).AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();
            services.ConfigureApplicationCookie(options => options.LoginPath = "/Admin/Login");
            services.AddControllersWithViews();
            services.AddRazorPages();

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.Configure<EmailingOptions>(Configuration.GetSection("EmailingSetting"));
            services.Configure<JobApplicationOptions>(Configuration.GetSection("JobApplicationOptions"));
            services.AddSingleton<IJobApplicationSender, JobApplicationSender>();
            // Add framework services.
            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            });
            services.Configure<WebEncoderOptions>(options =>
            {
                options.TextEncoderSettings = new TextEncoderSettings(UnicodeRanges.All);
            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseStatusCodePagesWithReExecute("/Home/Error");
                //app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseMiddleware<IpWhiteListMiddleware>("/Admin;/admin-content", Configuration["AdminWhiteList"]);
            app.UseForwardedHeaders();

            
            app.Use(async (context, next) =>
            {
                await next();
                switch (context.Response.StatusCode)
                {
                    case StatusCodes.Status404NotFound:
                        context.Request.Path = "/error";
                        break;
                    case StatusCodes.Status410Gone:
                        context.Request.Path = "/archived";
                        break;
                    default:
                        return;
                }
                await next();
            });
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "office",
                    template: "office/{location}",
                    defaults: new
                    {
                        controller = "home",
                        action = "office"
                    }
                );
                routes.MapRoute(
                    name: "vacancy",
                    template: "vacancy/{id}",
                    defaults: new
                    {
                        controller = "home",
                        action = "vacancy"
                    }
                );
                routes.MapRoute(
                    name: "vacancies",
                    template: "vacancies",
                    defaults: new
                    {
                        controller = "home",
                        action = "vacancies"
                    }
                );
                routes.MapRoute(
                    name: "pda",
                    template: "pda/{location?}",
                    defaults: new
                    {
                        controller = "home",
                        action = "pda"
                    }
                );
                routes.MapRoute(
                    name: "pda_ua",
                    template: "pda_ua",
                    defaults: new
                    {
                        controller = "home",
                        action = "pda_ua"
                    }
                );
                routes.MapRoute(
                    name: "pda_uz",
                    template: "pda_uz",
                    defaults: new
                    {
                        controller = "home",
                        action = "pda_uz"
                    }
                );
                routes.MapRoute(
                    name: "archived",
                    template: "archived",
                    defaults: new
                    {
                        controller = "home",
                        action = "archived"
                    }
                );
                routes.MapRoute(
                    name: "error",
                    template: "error",
                    defaults: new
                    {
                        controller = "home",
                        action = "error"
                    }
                );
                routes.MapRoute(
                    name: "sitemap",
                    template: "sitemap.xml",
                    defaults: new 
                    { controller = "Home", action = "Sitemap" 
                });

                routes.MapRoute(
                    name: "resource",
                    template: "content/{resource}",
                    defaults: new
                    {
                        controller = "home",
                        action = "staticcontent"
                    });
                routes.MapRoute(
                    name: "default",
                    template: "{controller=home}/{action=index}/{id?}");
            });
        }

        /*
                private void configureNLog(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
                {
                    LogManager.Configuration.Variables["connectionString"] = Configuration.GetConnectionString("DefaultConnection");
                    LogManager.Configuration.Variables["configDir"] = env.ContentRootPath;
                    //app.AddNLogWeb();
                }
        */
    }
}
