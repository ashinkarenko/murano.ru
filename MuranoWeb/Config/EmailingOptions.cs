﻿using MuranoWeb.Data.Entity;
using System.Collections.Generic;

namespace MuranoWeb.Config
{
    public class EmailingOptions
    {
        public string Server { get; set; }

        public int Port { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public Dictionary<string, string> EmailAddressByLocation { get; set; }
        public string DefaultEmailAddress { get; set; }

        public string[] GetAddress(Location location)
        {
            if (EmailAddressByLocation.ContainsKey(location.ToString()))
            {
                return EmailAddressByLocation[location.ToString()].Split(';');
            }
            return new string[] { DefaultEmailAddress };
        }

        public string MessageSubject { get; set; }
    }
}