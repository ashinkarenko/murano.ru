﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MuranoWeb.Data.Entity
{
    public class Vacancy
    {
        public int VacancyId { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Summary { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public string Requirements { get; set; }
        //public string AdditionalRequirements { get; set; }
        public Location Location { get; set; }

        public List<JobApplication> JobApplications { get; set; }
        public List<VacancyLog> Logs { get; set; }

        public bool InArchive { get; set; }

        public int Order { get; set; }
        public VacancyIcon Icon { get; set; }
        [Required]
        public int ExternalId { get; set; }
        public DateTime? ArchivedOn { get; set; }
        public int? SalaryFrom { get; set; }
        public int? SalaryTo { get; set; }
        public string Currency { get; set; }
    }
}