﻿using System.ComponentModel.DataAnnotations;

namespace MuranoWeb.Data.Entity
{
    public class VacancyIcon
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
