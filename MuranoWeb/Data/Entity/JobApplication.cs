﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MuranoWeb.Data.Entity
{
    public class JobApplication
    {
        public int Id { get; set; }

        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        public string Comment { get; set; }

        public string FileName { get; set; }
        public byte[] FileContent { get; set; }

        public int? VacancyId { get; set; }
        public Vacancy Vacancy { get; set; }

        public Location Location { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
