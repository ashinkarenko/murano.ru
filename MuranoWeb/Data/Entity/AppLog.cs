﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MuranoWeb.Data.Entity
{
    public class AppLog
    {
        public int Id { get; set; }
        public DateTime Logged { get; set; }
        [Required]
        [StringLength(50)]
        public string Level { get; set; }
        [Required]
        public string Message { get; set; }
        [Required]
        [StringLength(250)]
        public string Logger { get; set; }
        [Required]
        public string Callsite { get; set; }
        [Required]
        public string Exception { get; set; }
    }
}