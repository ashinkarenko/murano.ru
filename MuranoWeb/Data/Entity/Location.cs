﻿using System;
//using MuranoWeb.Data.Attributes;
using System.ComponentModel.DataAnnotations;

namespace MuranoWeb.Data.Entity
{
    [Flags]
    public enum Location
    {
        [Display(Name = "Санкт-Петербург")]
        SaintPetersburg = 1,
        [Display(Name = "Воронеж")]
        Voronezh = 2,
        [Display(Name = "Харьков")]
        Kharkov = 4,
        [Display(Name = "Ташкент")]
        Tashkent = 8
    }
}