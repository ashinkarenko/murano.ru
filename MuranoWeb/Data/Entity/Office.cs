﻿using System.ComponentModel.DataAnnotations;

namespace MuranoWeb.Data.Entity
{
    public class Office
    {
        public int id { get; set; }
        [Required]
        public string Country { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string Address { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Region { get; set; }
        public string OfficeImage { get; set; }
        public int OfficeImageWidth { get; set; }
        public double Lat { get; set; }
        public double Long { get; set; }
    }
}