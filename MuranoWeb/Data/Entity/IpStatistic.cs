﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MuranoWeb.Data.Entity
{
    public class IpStatistic
    {
        public int Id { get; set; }
        [Required]
        public string ClientIp { get; set; }
        [Required]
        public DateTime LastSubmitAt { get; set; }
        [Required]
        public UserStatus Status { get; set; }
        [Required]
        public int ShortRunStatistic { get; set; }

        public DateTime? BannedAt { get; set; }

        public int BansCount { get; set; }
    }
}
