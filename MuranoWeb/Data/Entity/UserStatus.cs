﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MuranoWeb.Data.Entity
{
    [Flags]
    public enum UserStatus
    {
        [Display(Name = "IP компании")]
        AlwaysActive = 0,
        [Display(Name = "Ок")]
        Ok = 1,
        [Display(Name = "Временно заблокирован")]
        TemporaryBanned = 2,
        [Display(Name = "Заблокирован")]
        Banned = 3
    }
}
