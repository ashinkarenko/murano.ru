﻿using MuranoWeb.Data.Attributes;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MuranoWeb.Data.Extensions
{
    public static class EnumExtensions
    {
        private static readonly ConcurrentDictionary<Type, EnumDisplayNames> _enumDisplayNames = new ConcurrentDictionary<Type, EnumDisplayNames>();

        public static string DisplayName(this Enum enumerationValue)
        {
            Type type = enumerationValue.GetType();
            if (!_enumDisplayNames.ContainsKey(type))
            {
                _enumDisplayNames.TryAdd(type, new EnumDisplayNames(type));
            }
            return _enumDisplayNames[type].GetDisplayName(enumerationValue);
        }


        private class EnumDisplayNames
        {
            private readonly Dictionary<Enum, string> _displayNames;

            public EnumDisplayNames(Type type)
            {
                if (!type.GetTypeInfo().IsEnum)
                {
                    throw new ArgumentException("The type should be enumeration", nameof(type));
                }
                _displayNames = new Dictionary<Enum, string>();
                foreach (MemberInfo enumMember in type.GetMembers())
                {
                    string displayName = getDisplayName(enumMember);
                    if (displayName == null)
                    {
                        continue;
                    }
                    var enumValue = (Enum)Enum.Parse(type, enumMember.Name);
                    _displayNames.Add(enumValue, displayName);
                }
            }

            public string GetDisplayName(Enum enumValue)
            {
                bool hasDisplayName = _displayNames.ContainsKey(enumValue);
                return hasDisplayName ? _displayNames[enumValue] : enumValue.ToString();
            }

            private static string getDisplayName(MemberInfo memberInfo)
            {
                var attr = (DisplayNameAttribute)memberInfo.GetCustomAttributes(typeof(DisplayNameAttribute), false).FirstOrDefault();
                return attr?.DisplayName;
            }
        }
    }
}