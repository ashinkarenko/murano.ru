﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MuranoWeb.Data.Entity;
using MuranoWeb.Models;

namespace MuranoWeb.Data
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Vacancy> Vacancies { get; set; }
        public DbSet<JobApplication> JobApplications { get; set; }
        public DbSet<Quote> Quotes { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<AppLog> AppLogs { get; set; }
        public DbSet<VacancyIcon> VacancyIcons { get; set; }
        public DbSet<IpStatistic> Statistics { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<ExternalIds> ExternalIds { get; set; }
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }
    }
}
