﻿using Microsoft.AspNetCore.Hosting;
using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Repositories;
using MuranoWeb.Utils;
using System;
using System.IO;
using System.Linq;

namespace MuranoWeb.Data.Migrations
{
    public class ImagesSeed
    {

        public static void Seed(IRepository<Image> repository, IWebHostEnvironment appEnvironment)
        {
            if (!repository.GetAll().Any())
            {
                foreach (var location in Enum.GetValues(typeof(Location)).Cast<Location>())
                {
                    seed(repository, appEnvironment, location);
                }
            }
        }

        private static void seed(IRepository<Image> repository, IWebHostEnvironment appEnvironment, Location location)
        {
            var images = Directory.EnumerateFiles(ImagesHelper.GetImagesDirectory(appEnvironment.WebRootPath, location)).
                OrderBy(x => x).
                Select((x, i) => new Image
                {
                    Name = Path.GetFileName(x),
                    Location = location,
                    SortOrder = i,
                    Show = true
                }).ToList();
            repository.AddRange(images).Wait();
        }
    }
}