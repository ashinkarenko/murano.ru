﻿using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace MuranoWeb.Data.Migrations
{
    public static class QuotesSeed
    {
        private static readonly List<Quote> _quotes = new List<Quote>
        {
            new Quote
            {
                Text = "Работа в Мурано дала мне то, о чем я раньше и мечтать не мог. Лучшая команда, в которой когда-либо доводилось работать. " +
                       "Рядом с гуру програмиирования, которые тут работают, " +
                       "прямо раскрываешь свои таланты, и всегда учишься новому. " +
                       "Да еще и кормят вкусно.",
                Author = "Иван Петров, ведущий разработчик"
            },
            new Quote
            {
                Text = "Два самых известных продукта, созданных в Университете Беркли — это UNIX и LSD. Это не может быть просто совпадением.",
                Author = "Jeremy S. Anderson"
            },
            new Quote
            {
                Text = "Итерация свойственна человеку, рекурсия божественна.",
                Author = "L. Peter Deutsch"
            },
            new Quote
            {
                Text = "Существует только два вида языков программирования: те, которые всех раздражают, и те, которые никто не использует.",
                Author = "Bjarne Stroustrup"
            },
            new Quote
            {
                Text = "Есть два способа написать программу без ошибок; работает только третий.",
                Author = "Alan J. Perlis"
            },
            new Quote
            {
                Text = "Программисты постоянно соревнуются со Вселенной: они пытаются создать всё более идиотоустойчивые программы," +
                       " а Вселенная создаёт всё более совершенных идиотов. Пока что Вселенная побеждает.",
                Author = "Rich Cook"
            }
        };

        public static void Seed(IRepository<Quote> repository)
        {
            if (!repository.GetAll().Any())
            {
                repository.AddRange(_quotes).Wait();
            }
        }
    }
}