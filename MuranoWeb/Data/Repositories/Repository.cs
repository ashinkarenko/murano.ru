﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MuranoWeb.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly ApplicationDbContext _db;

        public Repository(ApplicationDbContext db)
        {
            _db = db ?? throw new ArgumentNullException(nameof(db));
        }

        public ApplicationDbContext GetContext()
        {
            return _db;
        }

        public async Task<T> Add(T item)
        {
            await _db.Set<T>().AddAsync(item).ConfigureAwait(false);
            await _db.SaveChangesAsync().ConfigureAwait(false);
            return item;
        }

        public IQueryable<T> GetAll()
        {
            return _db.Set<T>();
        }

        public int GetCount()
        {
            return _db.Set<T>().Count();
        }

        public ValueTask<T> GetByIdAsync(int id)
        {
            return _db.Set<T>().FindAsync(id);
        }

        public T GetById(int id)
        {
            return _db.Set<T>().Find(id);
        }

        public async Task<T> Update(T item)
        {
            _db.Update(item);
            await _db.SaveChangesAsync().ConfigureAwait(false);
            return item;
        }

        public async Task AddRange(List<T> items)
        {
            await _db.AddRangeAsync(items).ConfigureAwait(false);
            await _db.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task Remove(T item)
        {
            _db.Set<T>().Remove(item);
            await _db.SaveChangesAsync().ConfigureAwait(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
        }
    }
}
