﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MuranoWeb.Data.Repositories
{
    public interface IRepository<T> : IDisposable
    {
        ValueTask<T> GetByIdAsync(int id);
        T GetById(int id);
        IQueryable<T> GetAll();
        Task<T> Add(T item);
        Task<T> Update(T item);
        Task AddRange(List<T> items);
        Task Remove(T item);
        int GetCount();
        ApplicationDbContext GetContext();
    }
}
