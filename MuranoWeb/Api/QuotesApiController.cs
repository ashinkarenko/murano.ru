﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MuranoWeb.Api
{
    [Authorize]
    [Route("admin/api/quotes")]
    public class QuotesApiController : Controller
    {
        private readonly IRepository<Quote> _repository;

        public QuotesApiController(IRepository<Quote> repository)
        {
            _repository = repository;
        }

        [HttpGet]
        public Task<List<Quote>> GetAll()
        {
            return _repository.GetAll().ToListAsync();
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            Quote quote = await _repository.GetByIdAsync(id);
            if (quote == null)
            {
                return NotFound();
            }
            return new ObjectResult(quote);
        }

        [HttpPost]
        public async Task<IActionResult> AddNew([FromBody] Quote model)
        {
            if (model == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            Quote created = await _repository.Add(model);

            return CreatedAtAction(nameof(QuotesApiController.GetById), new { created.Id });
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Quote model)
        {
            if (model == null || model.Id != id || !ModelState.IsValid)
            {
                return BadRequest();
            }

            await _repository.Update(model);

            return new NoContentResult();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            var quote = await _repository.GetByIdAsync(id);
            if (quote == null)
            {
                return NotFound();
            }

            await _repository.Remove(quote);

            return new OkResult();
        }
    }
}