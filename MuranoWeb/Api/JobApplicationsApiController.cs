﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MuranoWeb.Api.Models;
using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Repositories;
using MuranoWeb.Utils;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MuranoWeb.Api
{
    [Authorize]
    [Route("admin/api/applications")]
    public class JobApplicationsApiController : Controller
    {
        private readonly IRepository<JobApplication> _repository;
        private readonly int AppsPerPage = 20;

        public JobApplicationsApiController(IRepository<JobApplication> repository)
        {
            _repository = repository;
        }

        [HttpGet()]
        public Task<JobApplicationsModel> GetAll(int? page, Location? location, int? vacancyId)
        {
            IQueryable<JobApplication> applications = _repository.GetAll();
            if (location.HasValue)
            {
                applications = applications.Where(x => (x.Location & location) != 0);
            }
            if (vacancyId.HasValue)
            {
                applications = applications.Where(x => x.VacancyId == vacancyId);
            }

            int pageNum = page ?? 1;
            int pagesCount = (int)Math.Ceiling(applications.Count() / (float)AppsPerPage);
            if (pageNum > pagesCount)
                pageNum = pagesCount;
            if (pageNum < 1)
                pageNum = 1;


            var apps = applications.OrderByDescending(x => x.CreatedOn).Skip((pageNum - 1) * AppsPerPage).OrderByDescending(x => x.CreatedOn).Take(AppsPerPage).Select(x => new JobApplicationModel
            {
                Id = x.Id,
                VacancyId = x.VacancyId,
                FileName = x.FileName,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email,
                Location = x.Location,
                Comment = x.Comment,
                Date = x.CreatedOn,
                VacancyName = x.Vacancy.Title

            }).ToList();

            return Task.Run(() => new JobApplicationsModel()
            {
                CurrentPage = pageNum,
                PagesCount = pagesCount,
                Items = apps
            });
        }

        [HttpGet("{id}/rmapp")]
        public async Task<IActionResult> RemoveApp(int id)
        {

            var application = await _repository.GetByIdAsync(id);

            if (application != null)
            {
                await _repository.Remove(application);
                return Ok();
            }
            return NotFound();
        }


        [HttpGet("{id}/cv")]
        public async Task<IActionResult> GetCV(int id)
        {

            var application = await _repository.GetByIdAsync(id);
            if (application != null)
            {
                return File(application.FileContent, FileHelper.GetContentType(application.FileName), application.FileName);
            }
            return NotFound();
        }
    }
}