﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MuranoWeb.Api.Models;
using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MuranoWeb.Api
{
    [Authorize]
    [Route("admin/api/locations")]
    public class LocationsApiController : Controller
    {

        [HttpGet]
        public IEnumerable<LocationModel> GetAll()
        {
            return Enum.GetValues(typeof(Location)).Cast<Location>().Select(x => new LocationModel
            {
                Code = (int)x,
                Name = x.DisplayName()
            });
        }

    }
}
