﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using MuranoWeb.Models;

namespace MuranoWeb.Api
{
    [Authorize]
    [Route("admin/api/user")]
    public class UserApiController : Controller
    {
        private readonly SignInManager<User> _signInManager;

        public UserApiController(SignInManager<User> signInManager)
        {
            _signInManager = signInManager;
        }

        private string NameToCanonical(string name)
        {
            if (string.IsNullOrEmpty(name))
                return name;

            return name.Substring(0, 1).ToUpper() + name.Substring(1).ToLower();
        }
        private string NameFromEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
                return email;

            var atIndex = email.IndexOf('@');
            if (atIndex > 0)
                email = email.Substring(0, atIndex);
            var dotIndex = email.IndexOf('.');
            if (dotIndex <= 0 || dotIndex >= email.Length-1)
                return NameToCanonical(email);

            return NameToCanonical(email.Substring(0, dotIndex)) + " " +
                       NameToCanonical(email.Substring(dotIndex + 1));
            
        }

        [HttpGet]
        public IActionResult GetCurrent()
        {
            var userEmail = _signInManager.Context.User?.Identity?.Name ?? string.Empty;
            return new JsonResult(NameFromEmail(userEmail));
        }

    }
}
