﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MuranoWeb.Api.Models;
using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using MuranoWeb.Data.Repositories;

namespace MuranoWeb.Api
{
    [Authorize]
    [Route("admin/api/office")]
    public class OfficeApiController : Controller
    {
        private readonly IRepository<Office> _repository;

        public OfficeApiController(IRepository<Office> repository)
        {
            _repository = repository;
        }


        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            try
            {
                Office office = await _repository.GetByIdAsync(id).ConfigureAwait(false);


                if (office == null)
                {
                    return NotFound();
                }

                return new ObjectResult(office);
            }
            catch (Exception ex)
            {
                return Ok(ex.ToString());
            }

        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] Office model)
        {
            if (model == null || model.id != id || !ModelState.IsValid)
            {
                return new NoContentResult();
            }
            
            try
            {
                Office office = await _repository.GetByIdAsync(id);
                office.Country = model.Country;
                office.Region = model.Region;
                office.City = model.City;
                office.Address = model.Address;
                office.Zip = model.Zip;
                office.Phone = model.Phone;
                office.Email = model.Email;

                office.Lat = model.Lat;
                office.Long = model.Long;

                await _repository.Update(office);
                return new NoContentResult();
            }
            catch
            {
                return new StatusCodeResult(418);
            }
            
        }
    }
}
