﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Muranosoft.Indexing;
using MuranoWeb.Api.Models;
using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Extensions;
using MuranoWeb.Data.Repositories;
using MuranoWeb.Utils;
//using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace MuranoWeb.Api
{
    [Authorize]
    [Route("admin/api/vacancies")]
    public class VacanciesApiController : Controller
    {
        private readonly IRepository<Vacancy> _repository;
        private readonly IRepository<VacancyLog> _logRepository;
        private readonly IRepository<VacancyIcon> _iconRepository;
        private readonly IRepository<ExternalIds> _externalIdsRepository;
        //private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private readonly ILogger<VacanciesApiController> _logger;
        private readonly IWebHostEnvironment _env;

        private int GetNextExternalId()
        {
            return (_externalIdsRepository.Add(new ExternalIds()).Result).id;
        }

        private async void UpdateSitemap(Vacancy vacancy, string vacancyUrl, IndexingApiHelper.PublishType publishType)
        {
            // Update sitemap
            try
            {
                var url = new Uri(vacancyUrl);
                var sitemapLocale = LocalizedSitemap.GetLocaleFromHost(vacancyUrl);
                var siteServer = url.GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);
                var siteMapPath = Path.Combine(_env.WebRootPath, LocalizedSitemap.GetSitemapFileName(sitemapLocale));
                var sitemap = SitemapHelper.Load(siteMapPath);
                sitemap.UpdatePageRecord(vacancyUrl, 
                    publishType == IndexingApiHelper.PublishType.Update ? SitemapHelper.PageAction.Add : SitemapHelper.PageAction.Delete);
                sitemap.AddOrUpdatePage($"{siteServer}/vacancies");
                sitemap.AddOrUpdatePage($"{siteServer}/office/{vacancy.Location.ToString().ToLower()}", "0.80");
                await sitemap.SaveAsync(siteMapPath);
                
                LocalizedSitemap.UpdateSitemaps(_env.WebRootPath, sitemapLocale);
                _logger.LogInformation($"Sitemap is updated with action: {publishType}");
            }
            catch (Exception ex)
            {
                _logger.LogTrace(ex, "Sitemap update thrown an exception: ");
            }
        }

        private async void UpdateGoogleIndexing(Vacancy vacancy, string vacancyUrl, IndexingApiHelper.PublishType publishType)
        {
            
            try
            {
                var response = await IndexingApiHelper.PublishRequest(vacancyUrl, publishType, DateTime.Now);
                _logger.LogInformation($"Vacancy {vacancy.ExternalId} publish is completed with action: {publishType}");
                _logger.LogInformation(IndexingApiHelper.PublishUrlNotificationResponseToString(response));
            }
            catch (Exception ex)
            {
                _logger.LogTrace(ex, "Vacancy publish thrown an exception: ");
            }
            
        }
        private async void UpdateIndexing(Vacancy vacancy, IndexingApiHelper.PublishType publishType)
        {
            
            var siteServer = new Uri(HttpContext.Request.GetDisplayUrl())
                .GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped);
            var vacancyUrl = $"{siteServer}/vacancy/{vacancy.ExternalId}";

            await Task.Run(() => { UpdateSitemap(vacancy, vacancyUrl, publishType); });
#if DEBUG
            //_logger.Info("In debug mode - Indexing is not updated");
            _logger.LogInformation("In debug mode - Indexing is not updated");
#else
            _logger.LogInformation("Updating indexing");
            await Task.Run(() => { UpdateGoogleIndexing(vacancy, vacancyUrl, publishType); });
#endif            
        }

        public VacanciesApiController(IRepository<Vacancy> repository, 
            IRepository<VacancyLog> logRepository, 
            IRepository<VacancyIcon> iconRepository, 
            IRepository<ExternalIds> externalIdsRepository, 
            IWebHostEnvironment env,
            ILogger<VacanciesApiController> logger)
        {
            _repository = repository;
            _logRepository = logRepository;
            _iconRepository = iconRepository;
            _externalIdsRepository = externalIdsRepository;
            _env = env;
            _logger = logger;
        }

        private static int VacanciesPerPage = 20;
        [HttpGet]
        public Task<VacanciesModel> GetAll(Location? location, int? page, bool? archived, string search)
        {
            bool isArchived = archived == true;
            IQueryable<Vacancy> query = _repository.GetAll().Where(v=>v.InArchive == isArchived);
            if (search != null)
            {
                query = query.Where(v => v.Title.Contains(search));
            }
            if (location.HasValue)
            {
                query = query.Where(x => x.Location == location);
            }

            int currentPage = page ?? 1;
            int pagesCount = (archived == true) ? (int)Math.Ceiling(query.Count() / (float)VacanciesPerPage) : 1;
            if (currentPage > pagesCount)
                currentPage = pagesCount;
            if (currentPage < 1)
                currentPage = 1;

            if (archived == true)
            {
                query = query.OrderByDescending(v=>v.ArchivedOn).Skip((currentPage - 1) * VacanciesPerPage)
                    .OrderByDescending(v=>v.ArchivedOn)
                    .Take(VacanciesPerPage);
            }
            else
            {
                query = query.OrderBy(v => v.Order);
            }

            var vacancies = query.Select(x => new VacancyModel()
            {
                Id = x.VacancyId,
                Title = x.Title,
                Location = x.Location.DisplayName(),
                InArchive = x.InArchive,
                Ext = x.ExternalId,
                ArchivedOn = x.ArchivedOn
            }).ToList(); ;
            return Task.Run(() => new VacanciesModel()
            {
                CurrentPage = currentPage,
                PagesCount = pagesCount,
                Items = vacancies
            });
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            _logger.LogDebug("GetVacancyById: " + id);
            try
            {
                Vacancy vacancy = await _repository.GetByIdAsync(id).ConfigureAwait(false);


                if (vacancy == null)
                {
                    _logger.LogDebug("Vacancy " + id + " not found");
                    return NotFound();
                }
                await _repository.GetContext().Entry(vacancy).Reference("Icon").LoadAsync();

                return new ObjectResult(new VacancyFullModel
                {
                    Id = vacancy.VacancyId,
                    LocationEnum = vacancy.Location,
                    Location = vacancy.Location.DisplayName(),
                    Description = vacancy.Description,
                    Requirements = vacancy.Requirements,
                    Summary = vacancy.Summary,
                    Title = vacancy.Title,
                    InArchive = vacancy.InArchive,
                    IconId = vacancy.Icon.Id,
                    salaryFrom = vacancy.SalaryFrom,
                    salaryTo = vacancy.SalaryTo,
                    currency = vacancy.Currency
                });
            }
            catch (Exception ex)
            {
                _logger.LogDebug(ex, "GetVacancyById:");
                return Ok(ex.ToString());
            }

        }

        [HttpPost]
        public async Task<IActionResult> AddNew([FromBody] VacancyFullModel model)
        {
            if (model == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            VacancyIcon defaultIcon = await _iconRepository.GetByIdAsync(_iconRepository.GetAll().Min(x => x.Id));
            var icon = await _iconRepository.GetByIdAsync(model.IconId);

            Vacancy created = await _repository.Add(new Vacancy
            {
                Location = model.LocationEnum,
                Description = model.Description,
                Requirements = model.Requirements,
                Summary = model.Summary,
                Title = model.Title,
                Icon = icon ?? defaultIcon,
                ExternalId = GetNextExternalId(),
                SalaryFrom = model.salaryFrom,
                SalaryTo = model.salaryTo,
                Currency = model.currency?.Substring(0,3) ?? "USD"
            });

            await LogVacancy(created.VacancyId, VacancyAction.Create);
            UpdateIndexing(created, IndexingApiHelper.PublishType.Update);

            return CreatedAtAction(nameof(VacanciesApiController.GetById), new { Id = created.VacancyId });
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> InArchive(int id)
        {
            Vacancy vacancy = await _repository.GetByIdAsync(id);
            if (vacancy == null)
            {
                return NotFound();
            }

            vacancy.InArchive = true;
            vacancy.ArchivedOn = DateTime.Now;
            
            await _repository.Update(vacancy);

            await LogVacancy(vacancy.VacancyId, VacancyAction.Archive);
            UpdateIndexing(vacancy, IndexingApiHelper.PublishType.Delete);

            return new NoContentResult();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Update(int id, [FromBody] VacancyFullModel model)
        {
            if (model == null || model.Id != id || !ModelState.IsValid)
            {
                return new NoContentResult();
            }

            Vacancy vacancy = await _repository.GetByIdAsync(id);
            vacancy.Location = model.LocationEnum;
            vacancy.Description = model.Description;
            vacancy.Requirements = model.Requirements;
            vacancy.Summary = model.Summary;
            vacancy.Title = model.Title;
            vacancy.Icon = await _iconRepository.GetByIdAsync(model.IconId);
            vacancy.SalaryFrom = model.salaryFrom;
            vacancy.SalaryTo = model.salaryTo;
            vacancy.Currency = model.currency?.Substring(0, 3);
            await _repository.Update(vacancy);
            await LogVacancy(vacancy.VacancyId, VacancyAction.Update);
            _logger.LogInformation($"Vacancy is updated, id = {vacancy.VacancyId}, ExternalId = {vacancy.ExternalId} ");
            if (!vacancy.InArchive)
            {
                UpdateIndexing(vacancy, IndexingApiHelper.PublishType.Update);
            }

            return new NoContentResult();
        }

        [HttpOptions("{id}")]
        public async Task<IActionResult> Open(int id)
        {
            Vacancy vacancy = await _repository.GetByIdAsync(id);
            if (vacancy == null)
            {
                return NotFound();
            }

            vacancy.InArchive = false;
            vacancy.ArchivedOn = null;
            vacancy.ExternalId = GetNextExternalId();

            await _repository.Update(vacancy);

            await LogVacancy(vacancy.VacancyId, VacancyAction.Open);
            UpdateIndexing(vacancy, IndexingApiHelper.PublishType.Update);

            return new NoContentResult();
        }

        [HttpGet("{id}/log")]
        public Task<List<VacancyLogModel>> GetLogs(int id)
        {
            return _logRepository
                .GetAll()
                .Where(x => x.VacancyId == id)
                .Select(x => new VacancyLogModel()
                {
                    Action = x.Action.ToString(),
                    Date = x.Date,
                    User = x.User
                })
                .OrderByDescending(x => x.Date).Take(10).ToListAsync();
        }

        [HttpPatch("sort/{location}")]
        public async Task<IActionResult> UpdateOrder(Location location, [FromBody] int[] ids)
        {
            if (ids != null && ids.Any())
            {
                for (int i = 0; i < ids.Length; i++)
                {
                    var vacancy = await _repository.GetByIdAsync(ids[i]);
                    if (vacancy != null && vacancy.Order != i)
                    {
                        vacancy.Order = i;
                        await _repository.Update(vacancy);
                    }
                }
            }
            return Ok();
        }

        private async Task LogVacancy(int vacancyId, VacancyAction action)
        {
            await _logRepository.Add(new VacancyLog { VacancyId = vacancyId, User = User.Identity.Name, Action = action, Date = DateTime.Now }).ConfigureAwait(false);
        }
    }
}
