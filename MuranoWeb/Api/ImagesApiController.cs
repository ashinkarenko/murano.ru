﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MuranoWeb.Api.Models;
using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Repositories;
using MuranoWeb.Utils;
using NLog;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MuranoWeb.Api
{
    [Authorize]
    [Route("admin/api/images")]
    public class ImagesApiController : Controller
    {
        private readonly IWebHostEnvironment _appEnvironment;
        private readonly IRepository<Image> _imagesRepository;
        private readonly long MaxFileSize = 1024 * 1024;
        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public ImagesApiController(IWebHostEnvironment appEnvironment, IRepository<Image> imagesRepository)
        {
            _appEnvironment = appEnvironment;
            _imagesRepository = imagesRepository;
        }

        [HttpGet("{location}")]
        public Task<ImageModel[]> GetImages(Location location)
        {
            return _imagesRepository.GetAll().Where(x => x.Location == location).OrderBy(x => x.SortOrder).Select(x => new ImageModel
            {
                Id = x.Id,
                Show = x.Show,
                Url = ImagesHelper.GetImageUrl(x.Name, location)
            }).ToArrayAsync();
        }

        [HttpPost]
        public async Task<IActionResult> Upload(PhotosModel model)
        {
            _logger.Trace("Upload image");
            var location = (Location)model.location;
            _logger.Trace("Location: " + location.ToString());
            string directory = GetDirectory(location);
            var images = new List<Image>();
            var count = _imagesRepository.GetAll().Count(x => x.Location == location);

            foreach (IFormFile file in model.files)
            {
                if (file.Length > 0)
                {
                    int lastDot = file.FileName.LastIndexOf('.');
                    var fileName = ImagesHelper.MakeUniqueFileName(directory, file.FileName);
                    await using var stream = new MemoryStream((int)file.Length);
                    await file.CopyToAsync(stream);
                    ImagesHelper.ResizePhoto(stream, directory, fileName, MaxFileSize);
                    var previewFileName = await ImagesHelper.GeneratePreview(stream, _appEnvironment.WebRootPath, fileName, location);
                    images.Add(new Image { Location = location, Name = fileName, PreviewName = previewFileName, SortOrder = count++ });
                }
            }
            if (images.Any())
            {
                await _imagesRepository.AddRange(images);
            }

            return Ok();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            //var image = await _imagesRepository.GetById(id);
            var db = _imagesRepository.GetContext();
            var image = db.Images.FirstOrDefault(x => x.Id == id);

            if (image == null)
            {
                return NotFound();
            }

            //throw new System.Exception("Location: " + image.Location.ToString() + " Name: " + image.Name);

            FileHelper.DeleteFile(GetDirectory(image.Location), image.Name);
            if (image.PreviewName != null)
                FileHelper.DeleteFile(GetPreviewDirectory(image.Location), image.PreviewName);

            db.Images.Remove(image);
            await db.SaveChangesAsync();
            return Ok();

        }

        [HttpPatch("{location}/{id}")]
        public async Task<IActionResult> ToggleShow(Location location, int id)
        {
            var image = await _imagesRepository.GetByIdAsync(id);
            if (image == null)
            {
                return NotFound();
            }
            image.Show = !image.Show;
            await _imagesRepository.Update(image);
            return Ok();
        }

        [HttpPatch("sort/{location}")]
        public async Task<IActionResult> UpdateSortOrder(Location location, [FromBody] int[] ids)
        {
            if (ids != null && ids.Any())
            {
                for (int i = 0; i < ids.Length; i++)
                {
                    var image = await _imagesRepository.GetByIdAsync(ids[i]);
                    if (image != null && image.SortOrder != i)
                    {
                        image.SortOrder = i;
                        await _imagesRepository.Update(image);
                    }
                }
            }
            return Ok();
        }

        private string GetDirectory(Location location)
        {
            return ImagesHelper.GetImagesDirectory(_appEnvironment.WebRootPath, location);
        }

        private string GetPreviewDirectory(Location location)
        {
            return ImagesHelper.GetImagesPreviewDirectory(_appEnvironment.WebRootPath, location);
        }

    }
}
