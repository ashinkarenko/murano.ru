﻿using MuranoWeb.Data.Entity;
using System;

namespace MuranoWeb.Api.Models
{
    public class VacancyLogModel
    {
        /*
        public VacancyLogModel(VacancyLog log)
        {
            Action = log.Action.ToString();
            User = log.User;
            Date = log.Date;
        }
        */

        public string Action { get; set; }
        public string User { get; set; }
        public DateTime Date { get; set; }
    }
}
