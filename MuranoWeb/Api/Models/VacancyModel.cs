﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MuranoWeb.Api.Models
{
    public class VacancyModel
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Location { get; set; }
        public bool InArchive { get; set; }
        // ExternalId
        public int Ext { get; set; }
        public DateTime? ArchivedOn { get; set; }
    }
}
