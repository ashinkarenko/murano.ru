﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MuranoWeb.Api.Models
{
    public class PagingModel<T>
    {
        public int CurrentPage { get; set; }
        public int PagesCount { get; set; }
        public IEnumerable<T> Items { get; set; }

    }
}
