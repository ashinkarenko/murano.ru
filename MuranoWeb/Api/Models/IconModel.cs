﻿namespace MuranoWeb.Api.Models
{
    public class IconModel
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
    }
}