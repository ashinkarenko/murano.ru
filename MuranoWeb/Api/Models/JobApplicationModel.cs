﻿using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Extensions;
using System;

namespace MuranoWeb.Api.Models
{
    public class JobApplicationModel
    {
        public int Id { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Comment { get; set; }

        public string FileName { get; set; }

        public int? VacancyId { get; set; }
        public string VacancyName { get; set; }

        public DateTime Date { get; set; }

        public Location Location { get; set; }
        public string LocationName { get { return Location.DisplayName(); } }
    }
}
