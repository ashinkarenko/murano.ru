﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace MuranoWeb.Api.Models
{
    public class PhotosModel
    {
        public int location { get; set; }
        public List<IFormFile> files { get; set; }
    }
}
