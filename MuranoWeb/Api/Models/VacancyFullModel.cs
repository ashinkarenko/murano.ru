﻿using MuranoWeb.Data.Entity;
using MuranoWeb.Data.Extensions;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace MuranoWeb.Api.Models
{
    public class VacancyFullModel : VacancyModel
    {
        [Required]
        public string Summary { get; set; }
        [Required]
        public string Description { get; set; }
        public string Requirements { get; set; }
        public Location LocationEnum { get; set; }
        public object[] AvailableLocations { get { return Enum.GetValues(typeof(Location)).Cast<Location>().Select(x => new { Code = x, Name = x.DisplayName() }).ToArray(); } }
        public int IconId { get; set; }
        public int? salaryFrom { get; set; }
        public int? salaryTo { get; set; }
        public string currency { get; set; }
    }
}
