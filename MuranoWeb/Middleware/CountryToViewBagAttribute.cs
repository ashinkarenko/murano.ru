﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Http;

namespace MuranoWeb.Middleware
{
    public class CountryToViewBagAttribute : ActionFilterAttribute
    {
        private static string UkraineTestPattern = ".com.ua";
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            
            if (context.HttpContext.Request.Host.ToUriComponent().Contains(UkraineTestPattern))
            {
                (context.Controller as Microsoft.AspNetCore.Mvc.Controller).ViewBag.IsUkraine = true;
                return;
            }
            
            base.OnActionExecuting(context);
        }
    }
}
