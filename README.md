# muranosoft.ru

## UI

[Installing Node.js via package manager](https://nodejs.org/en/download/package-manager/)

``` bash
cd ./MuranoWeb/ui
```

#### Installation of dependencies
using [npm-install](https://docs.npmjs.com/cli/install)
``` bash
npm install
```

#### Run commands
using [run-script](https://docs.npmjs.com/cli/run-script) (alias run)
``` bash
npm run-script <command>
```
or
``` bash
npm run <command>
```

###### Supported commands
- `test` - run tests and watch
- `test-ci` - run tests
- `coverage` - run tests and coverage reporting
- `watch` - run watch and server
- `build` - run build
